This repository contains data and programs for the Laboratory Directed Research and Development program of Los Alamos National Laboratory project number 20190143ER "Objective Flow Topology".

The datasets of the test suite are in testSuite.

The programs for its generation and a readme explaining them are in src/generate.

The programs for the classical vector field topology and a readme explaining them are in src/classicFlowTopology.


NOTICE OF OSS COPYRIGHT ASSERTION  
TRIAD has asserted copyright on the software package entitled Objective Flow Topology, C19052.
   
ABSTRACT   
Flow topology is the most promising data reduction approach for flow fields because it breaks down even huge amounts of data into a compact, sparse, and easy to read description with little information loss. Unfortunately, it loses its physical meaning in time-dependent flows because it is not objective, which means that it is not independent of the frame of reference. In practice there are infinitely many frames of reference that a flow analyst would have to consider.  
 
The developers intend to distribute this software package under the BSD license. This code was developed using funding from the Laboratory Directed Research and Development Program which has concurred with the open source release and copyright assignment.
   
TRIAD acknowledges that it will comply with the DOE OSS policy as follows:        
a.       submit form DOE F 241.4 to the Energy Science and Technology Software Center (ESTSC),    
b.       provide the unique URL on the form for ESTSC to distribute, and    
c.       maintain an OSS Record available for inspection by DOE.      
    
Following is a table briefly summarizing information for this software package:        
·         Identifying Number: C19052
·         Software Name:  Objective Flow Topology
·         Export Control Review Information: U.S. Department of Commerce, EAR99   
·         B&R Code: YN0100000