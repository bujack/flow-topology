# this script takes the data as produced by ParaView's ParticleTracer if both its inputs are vti and transforms it into a flowmap in vti format. It also computes the eigenvalues, FTLE, our measure, the displacement field and angle and distance for the visualization through flowTopology/paraViewScripts/bivariateColormap.pvsm. The first input imageData.vti is one timestep of the original vector field for the structure of the output. The other inputs are the folders.

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import eig
import vtk.util.numpy_support as VN
import re
from shutil import copyfile

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)

def listdir(path):
  return [f for f in os.listdir(path) if not f.startswith('.') and f.find('spaceTime') == -1]
#-----------------------------------------------------------------------------------
# paramters


#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 4:
  print ("use: measure.py <filepath to file that contains the vector field imagedata forward> <filepath to file that contains the vector field imagedata backward> <filepath to folder that will contain the vti flowmap>")
  sys.exit()

input = sys.argv[1]
if not os.path.isdir(input):
  print ("enter filepath to folder that contains the vector field imagedata forward")
  sys.exit()
#print sorted(os.listdir(input))

inputBw = sys.argv[2]
if not os.path.isdir(input):
  print ("enter filepath to folder that contains the vector field imagedata backward")
  sys.exit()

output = sys.argv[3]
if not os.path.isdir(output):
  os.makedirs(output)
if not os.path.isdir(output + '/tempFmFw'):
  os.makedirs(output + '/tempFmFw')
if not os.path.isdir(output + '/tempFmBw'):
  os.makedirs(output + '/tempFmBw')
if not os.path.isdir(output + '/fmFw'):
  os.makedirs(output + '/FmFw')
if not os.path.isdir(output + '/fmBw'):
  os.makedirs(output + '/FmBw')
if not os.path.isdir(output + '/categoryFw'):
  os.makedirs(output + '/categoryFw')
if not os.path.isdir(output + '/categoryBw'):
  os.makedirs(output + '/categoryBw')
if not os.path.isdir(output + '/category'):
  os.makedirs(output + '/category')

scriptPath = os.path.dirname(sys.argv[0])

submitCommand = 'vtkpython ' + os.path.join(scriptPath, 'invertTime.py') + ' ' + input + ' ' + inputBw
#print ('--submitting: ' + submitCommand)
os.system(submitCommand)

submitCommand = 'pvpython ' + os.path.join(scriptPath, 'flowMapFromPv.py') + ' ' + input + ' ' + output + '/fmFw'
#print ('--submitting: ' + submitCommand)
os.system(submitCommand)

submitCommand = 'pvpython ' + os.path.join(scriptPath, 'flowMapFromPv.py') + ' ' + inputBw + ' ' + output + '/fmBw'
#print ('--submitting: ' + submitCommand)
os.system(submitCommand)

submitCommand = 'pvpython ' + os.path.join(scriptPath, 'flowMapInPv.py') + ' ' + input + ' ' + output + '/tempFmFw'
#print ('--submitting: ' + submitCommand)
os.system(submitCommand)

submitCommand = 'pvpython ' + os.path.join(scriptPath, 'flowMapInPv.py') + ' ' + inputBw + ' ' + output + '/tempFmBw'
#print ('--submitting: ' + submitCommand)
os.system(submitCommand)

submitCommand = 'vtkpython ' + os.path.join(scriptPath, 'advectScalar.py') + ' ' + output + '/fmFw/' + listdir(output + '/fmFw')[-1] + ' '  + output + '/tempFmFw ' + output + '/categoryFw'
#print ('--submitting: ' + submitCommand)
os.system(submitCommand)

submitCommand = 'vtkpython ' + os.path.join(scriptPath, 'advectScalar.py') + ' ' + output + '/fmBw/' + listdir(output + '/fmBw')[-1] + ' '  + output + '/tempFmBw ' + output + '/categoryBw'
#print ('--submitting: ' + submitCommand)
os.system(submitCommand)

# combine directions
for i in range(len(listdir(input))):
#  print (i)

  readerFw = vtk.vtkXMLImageDataReader()
  readerFw.SetFileName(output + '/categoryFw/advected' + str(i) + '.vti')
  readerFw.Update()

  readerBw = vtk.vtkXMLImageDataReader()
  readerBw.SetFileName(output + '/categoryBw/advected' + str((len(listdir(input))-1-i)) + '.vti')
  readerBw.Update()

  dataSet = vtk.vtkImageData()
  dataSet.CopyStructure(readerFw.GetOutput())

  measureFw = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("measure"))
  measureBw = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("measure"))
  measure = np.minimum(measureFw/max(1,i), measureBw/max(1,(len(listdir(input))-1-i)))

  strengthFw = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("strength"))
  strengthBw = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("strength"))
  strength = np.minimum(strengthFw/max(1,i), strengthBw/max(1,(len(listdir(input))-1-i)))

  categoryFw = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("category"))
  categoryBw = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("category"))
  category = [float('nan')] * len(categoryBw)
#  categoryOneSided = [float('nan')] * len(categoryBw)

  for j in range(len(categoryBw)):
    if categoryFw[j] > 0.5 and categoryBw[j] < -0.5:
      category[j] = 1
    elif categoryFw[j] < -0.5 and categoryBw[j] > 0.5:
      category[j] = -1
    elif abs(categoryFw[j]) < 0.5 and abs(categoryBw[j]) < 0.5:
      category[j] = 0
    else:
      category[j] = float('nan')
      strength[j] = 0

  categoryTemp = np.array(category).reshape(readerBw.GetOutput().GetExtent()[3]-readerBw.GetOutput().GetExtent()[2]+1, readerBw.GetOutput().GetExtent()[1]-readerBw.GetOutput().GetExtent()[0]+1)
  categoryBigger = np.full(categoryTemp.shape, float('nan'))
  for j in range(1,readerBw.GetOutput().GetExtent()[3]-readerBw.GetOutput().GetExtent()[2]):
    for k in range(1,readerBw.GetOutput().GetExtent()[1]-readerBw.GetOutput().GetExtent()[0]):
      if math.isnan(categoryBigger[j,k]):
        for l in range(-1,2,2):
          for m in range(-1,2,2):
            if not math.isnan(categoryTemp[j+l,k+m]):
              categoryBigger[j,k] = categoryTemp[j+l,k+m]


#  for j in range(len(categoryBw)):
#    if categoryBw[j] < -0.5:
#      categoryOneSided[j] = 1
#    if categoryFw[j] < -0.5:
#      categoryOneSided[j] = -1
#    if abs(categoryFw[j]) < 0.5 and abs(categoryBw[j]) < 0.5:
#      categoryOneSided[j] = 0
#    elif categoryBw[j] == 1:
#      categoryOneSided[j] = -1
#    if categoryFw[j] == -1:
#      categoryOneSided[j] = -1
#    elif categoryFw[j] == 0 and categoryBw[j] == 0:
#      categoryOneSided[j] = 0
#    else:
#      categoryOneSided[j] = float('nan')
#  categoryOneSided = categoryBw

  measureArray = VN.numpy_to_vtk(measure)
  measureArray.SetName("measure")
  dataSet.GetPointData().AddArray(measureArray)

  strengthArray = VN.numpy_to_vtk(strength)
  strengthArray.SetName("strength")
  dataSet.GetPointData().AddArray(strengthArray)

  categoryArray = VN.numpy_to_vtk(category)
  categoryArray.SetName("category")
  dataSet.GetPointData().AddArray(categoryArray)

  categoryBiggerArray = VN.numpy_to_vtk(categoryBigger.reshape(len(categoryBw)))
  categoryBiggerArray.SetName("categoryBigger")
  dataSet.GetPointData().AddArray(categoryBiggerArray)

#  categoryOneSidedArray = VN.numpy_to_vtk(categoryOneSided)
#  categoryOneSidedArray.SetName("categoryOneSided")
#  dataSet.GetPointData().AddArray(categoryOneSidedArray)

  writer = vtk.vtkXMLImageDataWriter()
  writer.SetInputData(dataSet)
  writer.SetFileName(output + '/category/advected' + str(i) + '.vti')
  writer.Write()

submitCommand = 'vtkpython ' + os.path.join(scriptPath, 'convertToSpaceTime.py') + ' ' + output + '/category ' + output + '/category'
#print ('--submitting: ' + submitCommand)
os.system(submitCommand)




