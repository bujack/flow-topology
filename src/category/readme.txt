
convertToSpaceTime.py is used for optimalFrames and explained there


flowMapFromPv.py makes a flowmap using ParaView's ParticleTracer. It also computes the eigenvalues, FTLE, our measure, the displacement field and angle and distance for the visualization through flowTopology/paraViewScripts/bivariateColormap.pvsm. 

vtkpython flowTopology/src/conver/flowMapFromPv.py inputFolder outputFolder


invertTime.py converts a dataset from our test suite into its inverse w.r.t. time. This allows backward integration in paraview. The time steps are sorted in reverse order and the vectors are replaced by their negative.

vtkpython flowTopology/src/invertTime.py flowTopology/data/saddleAt flowTopology/data/saddleAtBw


the second argument is the output directory it is optional