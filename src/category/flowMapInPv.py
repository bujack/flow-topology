# trace generated using paraview version 5.6.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
import os
import re
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)

def listdir(path):
  return [f for f in os.listdir(path) if not f.startswith('.') and f.find('spaceTime') == -1]

folder = sys.argv[1]
output = sys.argv[2]
fileList = sorted_aphanumeric(listdir(folder))
pathList = []
for i in fileList:
  filename, file_extension = os.path.splitext(i)
  if (file_extension == ".vti"):
    pathList.append(folder + '/' + i)

# create a new 'XML Image Data Reader'
data = XMLImageDataReader(FileName=pathList)
nameOfVectors = paraview.servermanager.Fetch(data).GetPointData().GetVectors().GetName()
data.PointArrayStatus = [nameOfVectors]

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1482, 1326]

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]

# get the material library
materialLibrary1 = GetMaterialLibrary()

# get color transfer function/color map for nameOfVectors
vectorsLUT = GetColorTransferFunction(nameOfVectors)

# get opacity transfer function/opacity map for nameOfVectors
vectorsPWF = GetOpacityTransferFunction(nameOfVectors)

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# create a new 'ParticleTracer'
particleTracer1 = ParticleTracer(Input=data,
    SeedSource=data)
particleTracer1.SelectInputVectors = ['POINTS', nameOfVectors]

# show data in view
particleTracer1Display = Show(particleTracer1, renderView1)

# trace defaults for the display properties.
particleTracer1Display.Representation = 'Surface'
particleTracer1Display.ColorArrayName = ['POINTS', nameOfVectors]
particleTracer1Display.LookupTable = vectorsLUT
particleTracer1Display.OSPRayScaleArray = 'AngularVelocity'
particleTracer1Display.OSPRayScaleFunction = 'PiecewiseFunction'
particleTracer1Display.SelectOrientationVectors = nameOfVectors
particleTracer1Display.ScaleFactor = 0.2
particleTracer1Display.SelectScaleArray = 'AngularVelocity'
particleTracer1Display.GlyphType = 'Arrow'
particleTracer1Display.GlyphTableIndexArray = 'AngularVelocity'
particleTracer1Display.GaussianRadius = 0.01
particleTracer1Display.SetScaleArray = ['POINTS', 'AngularVelocity']
particleTracer1Display.ScaleTransferFunction = 'PiecewiseFunction'
particleTracer1Display.OpacityArray = ['POINTS', 'AngularVelocity']
particleTracer1Display.OpacityTransferFunction = 'PiecewiseFunction'
particleTracer1Display.DataAxesGrid = 'GridAxesRepresentation'
particleTracer1Display.SelectionCellLabelFontFile = ''
particleTracer1Display.SelectionPointLabelFontFile = ''
particleTracer1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
particleTracer1Display.DataAxesGrid.XTitleFontFile = ''
particleTracer1Display.DataAxesGrid.YTitleFontFile = ''
particleTracer1Display.DataAxesGrid.ZTitleFontFile = ''
particleTracer1Display.DataAxesGrid.XLabelFontFile = ''
particleTracer1Display.DataAxesGrid.YLabelFontFile = ''
particleTracer1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
particleTracer1Display.PolarAxes.PolarAxisTitleFontFile = ''
particleTracer1Display.PolarAxes.PolarAxisLabelFontFile = ''
particleTracer1Display.PolarAxes.LastRadialAxisTextFontFile = ''
particleTracer1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(data, renderView1)

# show color bar/color legend
particleTracer1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# save data
SaveData(output + '/pv.vtk', proxy=particleTracer1, Writetimestepsasfileseries=1)

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]
renderView1.CameraParallelScale = 1.4142135623730951

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
