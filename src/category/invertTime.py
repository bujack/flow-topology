# this script converts a dataset from our test suite into its inverse w.r.t. time. This allows backward integration in paraview. The time steps are sorted in reverse order and the vectors are replaced by their negative.

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import eig
import vtk.util.numpy_support as VN
#from generateVectorField2D import constantTranslation
import re

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)

#-----------------------------------------------------------------------------------
# paramters


#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 2:
  print ("use: invertTime.py <filepath to folder that contains the data> <optional: filepath to folder that will contain the output>")
  sys.exit()

input = sys.argv[1]
if not os.path.isdir(input):
  print ("enter filepath to folder that contains the data as first argument")
  sys.exit()
#print sorted(os.listdir(input))

if len(sys.argv) == 3:
  output = sys.argv[2]
else:
  output = input
if not os.path.isdir(output):
  os.makedirs(output)


#-----------------------------------------------------------------------------------
# main program
timesteps = []
for i in sorted_aphanumeric(os.listdir(input)):
  filename, file_extension = os.path.splitext(i)
  if (file_extension == ".vti" and filename.find('spaceTime') == -1):
    timesteps.append(i)
#print timesteps

for i in range(len(timesteps)):
#for i in range(2):
#  print timesteps[i]
  reader = vtk.vtkXMLImageDataReader()
  reader.SetFileName(input + '/' + timesteps[i])
  reader.Update()
  data = reader.GetOutput()
  v = -VN.vtk_to_numpy(data.GetPointData().GetVectors())
  array = VN.numpy_to_vtk(v)
  array.SetName(data.GetPointData().GetVectors().GetName())
  data.GetPointData().AddArray(array)
  data.GetFieldData().RemoveArray("TimeValue")

  writer = vtk.vtkXMLImageDataWriter()
  writer.SetInputData(data)
  writer.SetFileName(output + '/bw' + str(timesteps[len(timesteps)-1-i]) )
  writer.Write()

timesteps = []
for i in sorted_aphanumeric(os.listdir(input)):
  filename, file_extension = os.path.splitext(i)
  if (file_extension == ".vtk" and filename.find('spaceTime') == -1):
    timesteps.append(i)
#print timesteps

for i in range(len(timesteps)):
  #for i in range(2):
  #  print timesteps[i]
  reader = vtk.vtkPolyDataReader()
  reader.SetFileName(input + '/' + timesteps[i])
  reader.Update()
  data = reader.GetOutput()
  v = -VN.vtk_to_numpy(data.GetPointData().GetArray("vectors"))
  array = VN.numpy_to_vtk(v)
  array.SetName("vectors")
  data.GetPointData().AddArray(array)

  writer = vtk.vtkPolyDataWriter()
  writer.SetInputData(data)
  writer.SetFileName(output + '/bw' + str(timesteps[len(timesteps)-1-i]))
  writer.Write()
