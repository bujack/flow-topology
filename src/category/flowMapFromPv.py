# this script takes the data as produced by ParaView's ParticleTracer if both its inputs are vti and transforms it into a flowmap in vti format. It also computes the eigenvalues, FTLE, our measure, the displacement field and angle and distance for the visualization through flowTopology/paraViewScripts/bivariateColormap.pvsm. The first input imageData.vti is one timestep of the original vector field for the structure of the output. The other inputs are the folders.

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import eig
import vtk.util.numpy_support as VN
import re
from numpy import linalg as LA

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)

def listdir(path):
  return [f for f in os.listdir(path) if not f.startswith('.') and f.find('spaceTime') == -1]

def computeE(data):
  v = VN.vtk_to_numpy(data.GetPointData().GetArray("finalPos"))
  v = v.reshape(data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1, 3)[:,:,0:2]

  spacing = np.array(data.GetSpacing())
  (gradientY, gradientX) = np.gradient(v,spacing[0],spacing[1],1)[0:2]
  gradientY = gradientY.reshape((dataSet.GetNumberOfPoints(),2))
  gradientX = gradientX.reshape((dataSet.GetNumberOfPoints(),2))

  gradient = np.stack([gradientX, gradientY], axis=2)

  measure = np.zeros(data.GetNumberOfPoints())
  lambda1 = np.zeros(data.GetNumberOfPoints())
  lambda2 = np.zeros(data.GetNumberOfPoints())
  v1 = np.zeros((data.GetNumberOfPoints(),3))
  v2 = np.zeros((data.GetNumberOfPoints(),3))
  ftle = np.zeros(data.GetNumberOfPoints())
  category = np.zeros(data.GetNumberOfPoints())
  strength = np.zeros(data.GetNumberOfPoints())

  for i in range(data.GetNumberOfPoints()):
    C = np.matmul(np.matrix.transpose(gradient[i]), gradient[i])
    w, v = LA.eig(C)
    (e1,e2) = (w[0],w[1])
    if e1 < -1e-10 or e2 < -1e-10: print (C, e1, e2)
    lambda1[i] = e1
    lambda2[i] = e2
    v1[i,0:2] = v[:,0]
    v2[i,0:2] = v[:,1]
    if e1 < e2:
      lambda1[i] = e2
      lambda2[i] = e1
      v1[i,0:2] = v[:,1]
      v2[i,0:2] = v[:,0]
    lambda1[i] = max(lambda1[i], 1e-10)
    lambda2[i] = max(lambda2[i], 1e-10)
    ftle[i] = np.log(math.sqrt(lambda1[i]))
    measure[i] = min(np.log(math.sqrt(lambda1[i])), -np.log(math.sqrt(lambda2[i])))
    category[i] = 0
    if lambda1[i] < 1:
      category[i] = -1
    if lambda2[i] > 1:
      category[i] = 1
#    if abs(np.log(math.sqrt(lambda1[i]))) < 1e-1 and abs(np.log(math.sqrt(lambda2[i]))) < 1e-1:
#      category[i] = float('nan')
    strength[i] = min(abs(np.log(math.sqrt(lambda1[i]))), abs(np.log(math.sqrt(lambda2[i]))))
#    category[i] = category[i] * strength

  lamba1Array = VN.numpy_to_vtk(lambda1)
  lamba1Array.SetName("lamba1")
  data.GetPointData().AddArray(lamba1Array)

  v1Array = VN.numpy_to_vtk(v1)
  v1Array.SetName("v1")
  data.GetPointData().AddArray(v1Array)

  v2Array = VN.numpy_to_vtk(v2)
  v2Array.SetName("v2")
  data.GetPointData().AddArray(v2Array)

  lambda2Array = VN.numpy_to_vtk(lambda2)
  lambda2Array.SetName("lamba2")
  data.GetPointData().AddArray(lambda2Array)

  measureArray = VN.numpy_to_vtk(measure)
  measureArray.SetName("measure")
  data.GetPointData().AddArray(measureArray)

  ftleArray = VN.numpy_to_vtk(ftle)
  ftleArray.SetName("ftle")
  data.GetPointData().AddArray(ftleArray)

  categoryArray = VN.numpy_to_vtk(category)
  categoryArray.SetName("category")
  data.GetPointData().AddArray(categoryArray)

  strengthArray = VN.numpy_to_vtk(strength)
  strengthArray.SetName("strength")
  data.GetPointData().AddArray(strengthArray)

  return data

def mapToBoundary(vectors, bounds, dataSet):
  for i in range(len(vectors)):
    closestBound = 0
    for j in range(4):
      if abs(vectors[i][int(j/2)]-bounds[j]) < abs(vectors[i][int(closestBound/2)]-bounds[closestBound]):
        closestBound = j
#    if abs(vectors[i][closestBound/2]-bounds[closestBound]) < 0.1 * (bounds[2*(closestBound/2)+1] - bounds[2*(closestBound/2)]):
    if np.linalg.norm(dataSet.GetPointData().GetVectors().GetTuple3(i)) > 1e-10:
      vectors[i][int(closestBound/2)] = bounds[closestBound]
  return vectors
#-----------------------------------------------------------------------------------
# paramters


#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 3:
  print ("use: flowMapFromParaView.py <filepath to file that contains the vector field imagedata> <filepath to folder that will contain the vti flowmap>")
  sys.exit()

input = sys.argv[1]
if not os.path.isdir(input):
  print ("enter filepath to folder that contains the vector field imagedata")
  sys.exit()
#print sorted(os.listdir(input))

output = sys.argv[2]
if not os.path.isdir(output):
  os.makedirs(output)

if len(sys.argv) > 3:
  useOnlyLastTimeStep = sys.argv[3]
else:
  useOnlyLastTimeStep = '0'

scriptPath = os.path.dirname(sys.argv[0])

submitCommand = 'pvpython ' + os.path.join(scriptPath, 'flowMapInPv.py') + ' ' + input + ' ' + output

print ('submitting: ', submitCommand)
os.system(submitCommand)

fileList = sorted_aphanumeric(listdir(input))
pathList = []
for i in fileList:
  filename, file_extension = os.path.splitext(i)
  if (file_extension == ".vti"):
    pathList.append(i)
#print (pathList)

if useOnlyLastTimeStep == '1':
  startTimeStep = len(pathList) - 1
else:
  startTimeStep = 0

reader = vtk.vtkDataSetReader()
reader.SetFileName(output + '/pv_0.vtk')
reader.Update()
data0 = reader.GetOutput()

for i in range(startTimeStep, len(pathList)):
#  print (i)
  filename, file_extension = os.path.splitext(pathList[i])

  readerImage = vtk.vtkXMLImageDataReader()
  readerImage.SetFileName(input + '/' + pathList[i])
  readerImage.Update()
  dataSet = readerImage.GetOutput()

  center = np.array([dataSet.GetBounds()[0] + 0.5 * (dataSet.GetBounds()[1]-dataSet.GetBounds()[0]), dataSet.GetBounds()[2] + 0.5 * (dataSet.GetBounds()[3]-dataSet.GetBounds()[2]),0])
  #print dataSet.GetBounds(), center

  reader = vtk.vtkDataSetReader()
  reader.SetFileName(output + '/pv_' + str(i) + '.vtk')
  reader.Update()
  data = reader.GetOutput()
#  print "\n", i, data

#  # this computes the angle and distance for visualizing the origin of the particles in a 2D colormap. needs to be commented out for category.py
#  angle = vtk.vtkDoubleArray()
#  angle.SetNumberOfTuples(data.GetNumberOfPoints())
#  angle.SetName("angle")
#  data.GetPointData().AddArray(angle)
#
#  distance = vtk.vtkDoubleArray()
#  distance.SetNumberOfTuples(data.GetNumberOfPoints())
#  distance.SetName("distance")
#  data.GetPointData().AddArray(distance)
#
#  for j in range(data.GetNumberOfPoints()):
#    index = int(data.GetPointData().GetArray("ParticleId").GetTuple1(j))
#    newVector = [data0.GetPoint(index)[0], data0.GetPoint(index)[1], 0]
#    angle.SetTuple1(j,math.atan2(newVector[1]-center[1],newVector[0]-center[0]))
#    distance.SetTuple1(j,np.linalg.norm(newVector-center))
#
#  writer = vtk.vtkDataSetWriter()
#  writer.SetInputData(data)
#  writer.SetFileName(output + '/angle/' + filename + '.vtk')
#  writer.Write()



  finalPos = vtk.vtkDoubleArray()
  finalPos.SetNumberOfComponents(3)
  finalPos.SetNumberOfTuples(dataSet.GetNumberOfPoints())
  finalPos.SetName("finalPos")
  dataSet.GetPointData().AddArray(finalPos)

  displacement = vtk.vtkDoubleArray()
  displacement.SetNumberOfComponents(3)
  displacement.SetNumberOfTuples(dataSet.GetNumberOfPoints())
  displacement.SetName("displacement")
  dataSet.GetPointData().AddArray(displacement)

  angle = vtk.vtkDoubleArray()
  angle.SetNumberOfTuples(dataSet.GetNumberOfPoints())
  angle.SetName("angle")
  dataSet.GetPointData().AddArray(angle)

  distance = vtk.vtkDoubleArray()
  distance.SetNumberOfTuples(dataSet.GetNumberOfPoints())
  distance.SetName("distance")
  dataSet.GetPointData().AddArray(distance)

  oldVectors = np.zeros((dataSet.GetNumberOfPoints(),3))
  newVectors = np.zeros((dataSet.GetNumberOfPoints(),3))

  for j in range(dataSet.GetNumberOfPoints()):
    oldVectors[j] = [dataSet.GetPoint(j)[0], dataSet.GetPoint(j)[1], 0]

  newVectors = mapToBoundary(oldVectors, dataSet.GetBounds(), dataSet)
  for j in range(data.GetNumberOfPoints()):
    index = int(data.GetPointData().GetArray("ParticleId").GetTuple1(j))
#    print index
    newVectors[index] = [data.GetPoint(j)[0], data.GetPoint(j)[1], 0]

  for j in range(dataSet.GetNumberOfPoints()):
    finalPos.SetTuple3(j, newVectors[j][0], newVectors[j][1], 0)
    displacement.SetTuple3(j, newVectors[j][0] - dataSet.GetPoint(j)[0], newVectors[j][1] - dataSet.GetPoint(j)[1], 0)
    angle.SetTuple1(j,math.atan2(newVectors[j][1]-center[1],newVectors[j][0]-center[0]))
    distance.SetTuple1(j,np.linalg.norm(newVectors[j]-center))
  oldVectors = newVectors

# the way we copy the output into a different order, makes the TimeValue move backward, which causes problems in paraview
  dataSet.GetFieldData().RemoveArray("TimeValue")
  dataSet = computeE(dataSet)

  writer = vtk.vtkXMLImageDataWriter()
  writer.SetInputData(dataSet)
  writer.SetFileName(output + '/fm' + filename + '.vti')
  writer.Write()

  os.remove(output + '/pv_' + str(i) + '.vtk')

if useOnlyLastTimeStep == '1':
  for i in range(len(pathList)-1):
    os.remove(output + '/pv_' + str(i) + '.vtk')
##-----------------------------------------------------------------------------------
## main program
#reader = vtk.vtkXMLImageDataReader()
#reader.SetFileName(imageDataInput)
#reader.Update()
##data = reader.GetOutput()
#
#dataSet = vtk.vtkImageData()
#dataSet.CopyStructure(reader.GetOutput())
##print dataSet
#center = np.array([dataSet.GetBounds()[0] + 0.5 * (dataSet.GetBounds()[1]-dataSet.GetBounds()[0]), dataSet.GetBounds()[2] + 0.5 * (dataSet.GetBounds()[3]-dataSet.GetBounds()[2]),0])
#print dataSet.GetBounds(), center
#
#finalPos = vtk.vtkDoubleArray()
#finalPos.SetNumberOfComponents(3)
#finalPos.SetNumberOfTuples(dataSet.GetNumberOfPoints())
#finalPos.SetName("finalPos")
#dataSet.GetPointData().AddArray(finalPos)
#
#displacement = vtk.vtkDoubleArray()
#displacement.SetNumberOfComponents(3)
#displacement.SetNumberOfTuples(dataSet.GetNumberOfPoints())
#displacement.SetName("displacement")
#dataSet.GetPointData().AddArray(displacement)
#
#angle = vtk.vtkDoubleArray()
#angle.SetNumberOfTuples(dataSet.GetNumberOfPoints())
#angle.SetName("angle")
#dataSet.GetPointData().AddArray(angle)
#
#distance = vtk.vtkDoubleArray()
#distance.SetNumberOfTuples(dataSet.GetNumberOfPoints())
#distance.SetName("distance")
#dataSet.GetPointData().AddArray(distance)
#
#oldVectors = np.zeros((dataSet.GetNumberOfPoints(),3))
#newVectors = np.zeros((dataSet.GetNumberOfPoints(),3))
#
#for j in range(dataSet.GetNumberOfPoints()):
#  oldVectors[j] = [dataSet.GetPoint(j)[0], dataSet.GetPoint(j)[1], 0]
#
#for i in sorted_aphanumeric(os.listdir(input)):
#  print i
#  filename, file_extension = os.path.splitext(i)
#  if (file_extension != ".vti"):
#    continue
#  reader = vtk.vtkDataSetReader()
#  reader.SetFileName(input + '/' + i)
#  reader.Update()
#  data = reader.GetOutput()
##  print "\n", i, data
#
#  newVectors = mapToBoundary(oldVectors, dataSet.GetBounds())
#  for j in range(data.GetNumberOfPoints()):
#    index = int(data.GetPointData().GetArray("ParticleId").GetTuple1(j))
##    print index
#    newVectors[index] = [data.GetPoint(j)[0], data.GetPoint(j)[1], 0]
#  for j in range(dataSet.GetNumberOfPoints()):
#    finalPos.SetTuple3(j, newVectors[j][0], newVectors[j][1], 0)
#    displacement.SetTuple3(j, newVectors[j][0] - dataSet.GetPoint(j)[0], newVectors[j][1] - dataSet.GetPoint(j)[1], 0)
#    angle.SetTuple1(j,math.atan2(newVectors[j][1]-center[1],newVectors[j][0]-center[0]))
#    distance.SetTuple1(j,np.linalg.norm(newVectors[j]-center))
#  oldVectors = newVectors
#
#  dataSet = computeE(dataSet)
#
#  writer = vtk.vtkXMLImageDataWriter()
#  writer.SetInputData(dataSet)
#  writer.SetFileName(output + '/' + filename + '.vti')
#  writer.Write()
