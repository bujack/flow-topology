# this script computes the maximum saddle and corresponding separartrices

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import eig
import vtk.util.numpy_support as VN
import re
from shutil import copyfile

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)

def listdir(path):
  return [f for f in os.listdir(path) if not f.startswith('.') and f.find('spaceTime') == -1]

#-----------------------------------------------------------------------------------
# paramters


#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 3:
  print ("use: surface.py <spacetimeData> <filepath to output folder>")
  sys.exit()

input = sys.argv[1]
inputVector = sys.argv[2]
output = sys.argv[3]
if not os.path.isdir(output):
  os.makedirs(output)


#-----------------------------------------------------------------------------------
# main program

readerFw = vtk.vtkXMLImageDataReader()
readerFw.SetFileName(input)
readerFw.Update()
data = readerFw.GetOutput()
#print data.GetPointData()

# extract maximum saddle
strength = VN.vtk_to_numpy(data.GetPointData().GetArray("measure")).reshape(data.GetExtent()[5]-data.GetExtent()[4]+1, data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1)
category = VN.vtk_to_numpy(data.GetPointData().GetArray("category")).reshape(data.GetExtent()[5]-data.GetExtent()[4]+1, data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1)
v1Fw = VN.vtk_to_numpy(data.GetPointData().GetArray("v1Fw")).reshape(data.GetExtent()[5]-data.GetExtent()[4]+1, data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1,3)
v1Bw = VN.vtk_to_numpy(data.GetPointData().GetArray("v1Bw")).reshape(data.GetExtent()[5]-data.GetExtent()[4]+1, data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1,3)
v2Fw = VN.vtk_to_numpy(data.GetPointData().GetArray("v2Fw")).reshape(data.GetExtent()[5]-data.GetExtent()[4]+1, data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1,3)
v2Bw = VN.vtk_to_numpy(data.GetPointData().GetArray("v2Bw")).reshape(data.GetExtent()[5]-data.GetExtent()[4]+1, data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1,3)
saddleStrength = np.zeros(strength.shape)
#saddleStrength = strength
saddleStrength[:,20:-20,20:-20] = (1-abs(category)[:,20:-20,20:-20]) * strength[:,20:-20,20:-20]
saddleStrength[:,20:-20,20:-20] = strength[:,20:-20,20:-20]

#saddleStrength[:,:,:] = strength[:,:,:]

#print saddleStrength.shape
#for i in range(10,data.GetExtent()[3]-data.GetExtent()[2]-10):
#  for j in range(10,data.GetExtent()[1]-data.GetExtent()[0]-10):
#    saddleStrength[:,i,j] = strength[:,i,j] - 0.25 * (strength[:,i+1,j]+strength[:,i-1,j]+strength[:,i,j+1]+strength[:,i,j-1])

strengthArray = VN.numpy_to_vtk(saddleStrength.reshape(data.GetNumberOfPoints()))
strengthArray.SetName("saddleStrength")
data.GetPointData().AddArray(strengthArray)

writer = vtk.vtkXMLImageDataWriter()
writer.SetInputData(data)
writer.SetFileName(output + '/spacetime.vti')
writer.Write()

polyData = vtk.vtkPolyData()
points = vtk.vtkPoints()
cells = vtk.vtkCellArray()
polyData.SetPoints(points)
#polyData.SetPolys(cells)

polyDataBw1 = vtk.vtkPolyData()
pointsBw1 = vtk.vtkPoints()
polyDataBw1.SetPoints(pointsBw1)

polyDataBw2 = vtk.vtkPolyData()
pointsBw2 = vtk.vtkPoints()
polyDataBw2.SetPoints(pointsBw2)

polyDataFw1 = vtk.vtkPolyData()
pointsFw1 = vtk.vtkPoints()
polyDataFw1.SetPoints(pointsFw1)

polyDataFw2 = vtk.vtkPolyData()
pointsFw2 = vtk.vtkPoints()
polyDataFw2.SetPoints(pointsFw2)

polyDataPathLine = vtk.vtkPolyData()
pointsPathLine = vtk.vtkPoints()
polyDataPathLine.SetPoints(pointsPathLine)

for i in range(1,data.GetExtent()[5]-data.GetExtent()[4]):
  strengthI = np.zeros((data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1))
  strengthI[:,:] = saddleStrength[i,:,:]
  maxIndexI = np.nanargmax(strengthI, axis=None)
  maxIndex = np.ravel_multi_index([i, np.unravel_index(maxIndexI, strengthI.shape)[0], np.unravel_index(maxIndexI, strengthI.shape)[1]], dims=strength.shape)
#  maxIndex = np.ravel_multi_index([i, 40, 40], dims=strength.shape)
  point = data.GetPoint(maxIndex)
  points.InsertNextPoint(point)
#  print maxIndex, [np.unravel_index(maxIndex, strength.shape),0]
#  print v1Bw[i,np.unravel_index(maxIndexI, strengthI.shape)[0],np.unravel_index(maxIndexI, strengthI.shape)[1],0]
  multiIndex = (i,np.unravel_index(maxIndexI, strengthI.shape)[0],np.unravel_index(maxIndexI, strengthI.shape)[1])
#  print multiIndex, v1Bw[multiIndex][0], v1Bw[multiIndex][1]
  point1 = (point[0]+0.01*v1Bw[multiIndex][0],point[1]+0.01*v1Bw[multiIndex][1],point[2])
  point2 = (point[0]-0.01*v1Bw[multiIndex][0],point[1]-0.01*v1Bw[multiIndex][1],point[2])
  pointsBw1.InsertNextPoint(point1)
  pointsBw2.InsertNextPoint(point2)
  if i == 9:
    pointsPathLine.InsertNextPoint(point1)
    pointsPathLine.InsertNextPoint(point2)
  point1 = (point[0]+0.01*v1Fw[multiIndex][0],point[1]+0.01*v1Fw[multiIndex][1],point[2])
  point2 = (point[0]-0.01*v1Fw[multiIndex][0],point[1]-0.01*v1Fw[multiIndex][1],point[2])
  pointsFw1.InsertNextPoint(point1)
  pointsFw2.InsertNextPoint(point2)
  if i == 9:
    pointsPathLine.InsertNextPoint(point1)
    pointsPathLine.InsertNextPoint(point2)

  if i > 1:
    line = vtk.vtkLine()
    line.GetPointIds().SetId( 0, points.GetNumberOfPoints() - 1)
    line.GetPointIds().SetId( 1, points.GetNumberOfPoints() - 2)
    cells.InsertNextCell(line)
#print polyData.GetNumberOfPoints()

writer = vtk.vtkPolyDataWriter()
writer.SetInputData(polyData)
writer.SetFileName(output + '/saddleCore.vtk')
writer.Write()

writer = vtk.vtkPolyDataWriter()
writer.SetInputData(polyDataPathLine)
writer.SetFileName(output + '/seeds.vtk')
writer.Write()
#
## forward
#rake = vtk.vtkLineSource()
#rake.SetPoint1(0, -1, 0)
#rake.SetPoint2(0, 1, 0)
#rake.SetResolution(20)
#rake.Update()
#print rake.GetOutput().GetNumberOfPoints()

readerFw = vtk.vtkXMLImageDataReader()
readerFw.SetFileName(inputVector)
readerFw.Update()

streamTracer = vtk.vtkStreamTracer()
streamTracer.SetInputData(readerFw.GetOutput())
streamTracer.SetSourceData(polyDataFw1)
streamTracer.SetIntegratorTypeToRungeKutta45()
streamTracer.SetInterpolatorTypeToDataSetPointLocator()
streamTracer.SetSurfaceStreamlines(0)
streamTracer.SetInitialIntegrationStep(1)
streamTracer.SetIntegrationDirectionToForward()
streamTracer.SetComputeVorticity(0)
streamTracer.SetMinimumIntegrationStep(0.001)
streamTracer.SetMaximumIntegrationStep(0.5)
streamTracer.SetMaximumNumberOfSteps(10000)
streamTracer.SetMaximumPropagation(100)
streamTracer.Update()
#print (streamTracer.GetOutput().GetNumberOfPoints())

#writer = vtk.vtkPolyDataWriter()
#writer.SetInputData(streamTracer.GetOutput())
#writer.SetFileName(output + '/LinesFw.vtk')
#writer.Write()

streamSurfacer = vtk.vtkRuledSurfaceFilter()
streamSurfacer.SetInputConnection(streamTracer.GetOutputPort());
streamSurfacer.SetRuledModeToPointWalk();
streamSurfacer.PassLinesOn();
streamSurfacer.SetOffset(0);
streamSurfacer.SetOnRatio(1);
streamSurfacer.SetDistanceFactor(10.0);
streamSurfacer.Update()
surfaceFw1 = streamSurfacer.GetOutput()

streamTracer = vtk.vtkStreamTracer()
streamTracer.SetInputData(readerFw.GetOutput())
streamTracer.SetSourceData(polyDataFw2)
streamTracer.SetIntegratorTypeToRungeKutta45()
streamTracer.SetInterpolatorTypeToDataSetPointLocator()
streamTracer.SetSurfaceStreamlines(0)
streamTracer.SetInitialIntegrationStep(1)
streamTracer.SetIntegrationDirectionToForward()
streamTracer.SetComputeVorticity(0)
streamTracer.SetMinimumIntegrationStep(0.001)
streamTracer.SetMaximumIntegrationStep(0.5)
streamTracer.SetMaximumNumberOfSteps(10000)
streamTracer.SetMaximumPropagation(100)
streamTracer.Update()
#print streamTracer.GetOutput().GetNumberOfPoints()

#writer = vtk.vtkPolyDataWriter()
#writer.SetInputData(streamTracer.GetOutput())
#writer.SetFileName(output + '/LinesFw.vtk')
#writer.Write()

streamSurfacer = vtk.vtkRuledSurfaceFilter()
streamSurfacer.SetInputConnection(streamTracer.GetOutputPort());
streamSurfacer.SetRuledModeToPointWalk();
streamSurfacer.PassLinesOn();
streamSurfacer.SetOffset(0);
streamSurfacer.SetOnRatio(1);
streamSurfacer.SetDistanceFactor(10.0);
streamSurfacer.Update()
surfaceFw2 = streamSurfacer.GetOutput()

appendFilter = vtk.vtkAppendPolyData()
appendFilter.AddInputData(surfaceFw1)
appendFilter.AddInputData(surfaceFw2)
appendFilter.Update()

writer = vtk.vtkPolyDataWriter()
writer.SetInputData(appendFilter.GetOutput())
writer.SetFileName(output + '/surfaceFw.vtk')
writer.Write()

# backward
streamTracer = vtk.vtkStreamTracer()
streamTracer.SetInputData(readerFw.GetOutput())
streamTracer.SetSourceData(polyDataBw1)
streamTracer.SetIntegratorTypeToRungeKutta45()
streamTracer.SetInterpolatorTypeToDataSetPointLocator()
streamTracer.SetSurfaceStreamlines(0)
streamTracer.SetInitialIntegrationStep(1)
streamTracer.SetIntegrationDirectionToBackward()
streamTracer.SetComputeVorticity(0)
streamTracer.SetMinimumIntegrationStep(0.001)
streamTracer.SetMaximumIntegrationStep(0.5)
streamTracer.SetMaximumNumberOfSteps(10000)
streamTracer.SetMaximumPropagation(100)
streamTracer.Update()
#print streamTracer.GetOutput().GetNumberOfPoints()

#writer = vtk.vtkPolyDataWriter()
#writer.SetInputData(streamTracer.GetOutput())
#writer.SetFileName(output + '/LinesFw.vtk')
#writer.Write()

streamSurfacer = vtk.vtkRuledSurfaceFilter()
streamSurfacer.SetInputConnection(streamTracer.GetOutputPort());
streamSurfacer.SetRuledModeToPointWalk();
streamSurfacer.PassLinesOn();
streamSurfacer.SetOffset(0);
streamSurfacer.SetOnRatio(1);
streamSurfacer.SetDistanceFactor(10.0);
streamSurfacer.Update()
surfaceBw1 = streamSurfacer.GetOutput()

# backward 2
streamTracer = vtk.vtkStreamTracer()
streamTracer.SetInputData(readerFw.GetOutput())
streamTracer.SetSourceData(polyDataBw2)
streamTracer.SetIntegratorTypeToRungeKutta45()
streamTracer.SetInterpolatorTypeToDataSetPointLocator()
streamTracer.SetSurfaceStreamlines(0)
streamTracer.SetInitialIntegrationStep(1)
streamTracer.SetIntegrationDirectionToBackward()
streamTracer.SetComputeVorticity(0)
streamTracer.SetMinimumIntegrationStep(0.001)
streamTracer.SetMaximumIntegrationStep(0.5)
streamTracer.SetMaximumNumberOfSteps(10000)
streamTracer.SetMaximumPropagation(100)
streamTracer.Update()
#print streamTracer.GetOutput().GetNumberOfPoints()

#writer = vtk.vtkPolyDataWriter()
#writer.SetInputData(streamTracer.GetOutput())
#writer.SetFileName(output + '/LinesFw.vtk')
#writer.Write()

streamSurfacer = vtk.vtkRuledSurfaceFilter()
streamSurfacer.SetInputConnection(streamTracer.GetOutputPort());
streamSurfacer.SetRuledModeToPointWalk();
streamSurfacer.PassLinesOn();
streamSurfacer.SetOffset(0);
streamSurfacer.SetOnRatio(1);
streamSurfacer.SetDistanceFactor(10.0);
streamSurfacer.Update()
surfaceBw2 = streamSurfacer.GetOutput()

appendFilter = vtk.vtkAppendPolyData()
appendFilter.AddInputData(surfaceBw1)
appendFilter.AddInputData(surfaceBw2)
appendFilter.Update()

writer = vtk.vtkPolyDataWriter()
writer.SetInputData(appendFilter.GetOutput())
writer.SetFileName(output + '/surfaceBw.vtk')
writer.Write()

# pathlines
streamTracer = vtk.vtkStreamTracer()
streamTracer.SetInputData(readerFw.GetOutput())
streamTracer.SetSourceData(polyDataPathLine)
streamTracer.SetIntegratorTypeToRungeKutta45()
streamTracer.SetInterpolatorTypeToDataSetPointLocator()
streamTracer.SetSurfaceStreamlines(0)
streamTracer.SetInitialIntegrationStep(1)
streamTracer.SetIntegrationDirectionToBoth()
streamTracer.SetComputeVorticity(0)
streamTracer.SetMinimumIntegrationStep(0.001)
streamTracer.SetMaximumIntegrationStep(0.5)
streamTracer.SetMaximumNumberOfSteps(10000)
streamTracer.SetMaximumPropagation(100)
streamTracer.Update()
#print streamTracer.GetOutput()

pathLines = vtk.vtkPolyData()
points = vtk.vtkPoints()
pathLines.SetPoints(points)
#print pathLines

for i in range(streamTracer.GetOutput().GetNumberOfPoints()):
  point = streamTracer.GetOutput().GetPoint(i)
  newPoint = (point[0], point[1], 0)
  points.InsertNextPoint(newPoint)
streamTracer.GetOutput().SetPoints(points)
#print streamTracer.GetOutput()

writer = vtk.vtkPolyDataWriter()
writer.SetInputData(streamTracer.GetOutput())
writer.SetFileName(output + '/pathLines.vtk')
writer.Write()

## backward
#rake = vtk.vtkLineSource()
#rake.SetPoint1(-1, 0.5, 20)
#rake.SetPoint2(1, 0.5, 20)
#rake.SetResolution(20)
#rake.Update()
#print rake.GetOutput().GetNumberOfPoints()
#
#streamTracer = vtk.vtkStreamTracer()
#streamTracer.SetInputData(readerFw.GetOutput())
#streamTracer.SetSourceData(rake.GetOutput())
#streamTracer.SetIntegratorTypeToRungeKutta45()
#streamTracer.SetInterpolatorTypeToDataSetPointLocator()
#streamTracer.SetSurfaceStreamlines(0)
#streamTracer.SetInitialIntegrationStep(1)
#streamTracer.SetIntegrationDirectionToBoth()
#streamTracer.SetComputeVorticity(0)
#streamTracer.SetMinimumIntegrationStep(0.001)
#streamTracer.SetMaximumIntegrationStep(0.5)
#streamTracer.SetMaximumNumberOfSteps(10000)
#streamTracer.SetMaximumPropagation(100)
#streamTracer.Update()
#print streamTracer.GetOutput().GetNumberOfPoints()
#
#streamSurfacer = vtk.vtkRuledSurfaceFilter()
#streamSurfacer.SetInputConnection(streamTracer.GetOutputPort());
#streamSurfacer.SetRuledModeToPointWalk();
#streamSurfacer.PassLinesOn();
#streamSurfacer.SetOffset(0);
#streamSurfacer.SetOnRatio(1);
#streamSurfacer.SetDistanceFactor(50.0);
#streamSurfacer.Update()
#surfaceBw = streamSurfacer.GetOutput()
#writer = vtk.vtkPolyDataWriter()
#
#writer.SetInputData(surfaceBw)
#writer.SetFileName(output + '/surfaceBw.vtk')
#writer.Write()
#
## intersect
#intersect = vtk.vtkIntersectionPolyDataFilter()
#intersect.SetInputData(0, surfaceFw)
#intersect.SetInputData(1, surfaceBw)
#intersect.SplitFirstOutputOff()
#intersect.SplitSecondOutputOff()
#intersect.Update()
#print intersect.GetOutput().GetNumberOfPoints()
#
#writer.SetInputData(intersect.GetOutput())
#writer.SetFileName(output + '/surfaceIntersect.vtk')
#writer.Write()
