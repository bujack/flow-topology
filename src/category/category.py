# this script takes the data as produced by ParaView's ParticleTracer if both its inputs are vti and transforms it into a flowmap in vti format. It also computes the eigenvalues, FTLE, our measure, the displacement field and angle and distance for the visualization through flowTopology/paraViewScripts/bivariateColormap.pvsm. The first input imageData.vti is one timestep of the original vector field for the structure of the output. The other inputs are the folders.

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import eig
import vtk.util.numpy_support as VN
import re
from shutil import copyfile

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)

def listdir(path):
  return [f for f in os.listdir(path) if not f.startswith('.') and f.find('spaceTime') == -1]
#-----------------------------------------------------------------------------------
# paramters


#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 4:
  print ("use: category.py <filepath to file that contains the vector field imagedata forward> <filepath to file that contains the vector field imagedata backward> <filepath to folder that will contain the vti flowmap>")
  sys.exit()

input = sys.argv[1]
if not os.path.isdir(input):
  print ("enter filepath to folder that contains the vector field imagedata forward")
  sys.exit()
#print sorted(os.listdir(input))

inputBw = sys.argv[2]
if not os.path.isdir(input):
  print ("enter filepath to folder that contains the vector field imagedata backward")
  sys.exit()

output = sys.argv[3]
if not os.path.isdir(output):
  os.makedirs(output)
if not os.path.isdir(output + '/tempData'):
  os.makedirs(output + '/tempData')
if not os.path.isdir(output + '/tempFm'):
  os.makedirs(output + '/tempFm')
if not os.path.isdir(output + '/categoryFw'):
  os.makedirs(output + '/categoryFw')
if not os.path.isdir(output + '/categoryBw'):
  os.makedirs(output + '/categoryBw')
if not os.path.isdir(output + '/category'):
  os.makedirs(output + '/category')

scriptPath = os.path.dirname(sys.argv[0])

submitCommand = 'vtkpython ' + os.path.join(scriptPath, 'convertToSpaceTime.py') + ' ' + input + ' ' + input
print ('-submitting: ' + submitCommand)
os.system(submitCommand)

submitCommand = 'vtkpython ' + os.path.join(scriptPath, 'invertTime.py') + ' ' + input + ' ' + inputBw
print ('-submitting: ' + submitCommand)
os.system(submitCommand)

print ('-computing flowmap')
for i in range(len(listdir(input))):
#for i in range(10,11):

  # flowmap forward from i..end
  if i == len(listdir(input))-1:
    readerFw = vtk.vtkXMLImageDataReader()
    readerFw.SetFileName(input + '/' + listdir(input)[i])
    readerFw.Update()

    dataSet = vtk.vtkImageData()
    dataSet.CopyStructure(readerFw.GetOutput())
    measure = [0] * readerFw.GetOutput().GetNumberOfPoints()
    strength = [0] * readerFw.GetOutput().GetNumberOfPoints()
    category = [float('nan')] * readerFw.GetOutput().GetNumberOfPoints()
#    ftle
#    e1
#    e2
#    v1
#    v2

    measureArray = VN.numpy_to_vtk(measure)
    measureArray.SetName("measure")
    dataSet.GetPointData().AddArray(measureArray)

    strengthArray = VN.numpy_to_vtk(strength)
    strengthArray.SetName("strength")
    dataSet.GetPointData().AddArray(strengthArray)

    categoryArray = VN.numpy_to_vtk(category)
    categoryArray.SetName("category")
    dataSet.GetPointData().AddArray(categoryArray)

    writer = vtk.vtkXMLImageDataWriter()
    writer.SetInputData(dataSet)
    writer.SetFileName(output + '/categoryFw/categoryFw' + str(i) + '.vti')
    writer.Write()
  else:
    fileList = sorted_aphanumeric(listdir(input))
    for j in range(i,len(fileList)):
      filename, file_extension = os.path.splitext(fileList[j])
      if (file_extension == ".vti" and filename.find('spaceTime') == -1):
        copyfile(input + '/' + fileList[j], output + '/tempData/' + fileList[j])

    submitCommand = 'pvpython ' + os.path.join(scriptPath, 'flowMapFromPv.py') + ' ' + output + '/tempData' + ' ' + output + '/tempFm' + ' 1'
    #print 'submitting: ' + submitCommand
    os.system(submitCommand)

    copyfile(output + '/tempFm/' + sorted_aphanumeric(listdir(output + '/tempFm'))[-1], output + '/categoryFw/categoryFw' + str(i) + '.vti')

    for j in listdir(output + '/tempData'):
      os.remove(output + '/tempData/' + j)
    for j in listdir(output + '/tempFm'):
      os.remove(output + '/tempFm/' + j)

  # flowmap backward from i..0
  if i == 0:
    readerBw = vtk.vtkXMLImageDataReader()
    readerBw.SetFileName(inputBw + '/' + listdir(inputBw)[i])
    readerBw.Update()

    dataSet = vtk.vtkImageData()
    dataSet.CopyStructure(readerBw.GetOutput())
    measure = [0] * readerBw.GetOutput().GetNumberOfPoints()
    strength = [0] * readerBw.GetOutput().GetNumberOfPoints()
    category = [float('nan')] * readerBw.GetOutput().GetNumberOfPoints()

    measureArray = VN.numpy_to_vtk(measure)
    measureArray.SetName("measure")
    dataSet.GetPointData().AddArray(measureArray)

    strengthArray = VN.numpy_to_vtk(strength)
    strengthArray.SetName("strength")
    dataSet.GetPointData().AddArray(strengthArray)

    categoryArray = VN.numpy_to_vtk(category)
    categoryArray.SetName("category")
    dataSet.GetPointData().AddArray(categoryArray)

    writer = vtk.vtkXMLImageDataWriter()
    writer.SetInputData(dataSet)
    writer.SetFileName(output + '/categoryBw/categoryBw' + str(i) + '.vti')
    writer.Write()
  else:
    fileList = sorted_aphanumeric(listdir(inputBw))
    for j in range(len(fileList)-1-i, len(fileList)):
      filename, file_extension = os.path.splitext(fileList[j])
      if (file_extension == ".vti" and filename.find('spaceTime') == -1):
        copyfile(inputBw + '/' + fileList[j], output + '/tempData/' + fileList[j])

    submitCommand = 'pvpython ' + os.path.join(scriptPath, 'flowMapFromPv.py') + ' ' + output + '/tempData' + ' ' + output + '/tempFm' + ' 1'
    #print 'submitting: ' + submitCommand
    os.system(submitCommand)

    copyfile(output + '/tempFm/' + sorted_aphanumeric(listdir(output + '/tempFm'))[-1], output + '/categoryBw/categoryBw' + str(i) + '.vti')

    for j in listdir(output + '/tempData'):
      os.remove(output + '/tempData/' + j)
    for j in listdir(output + '/tempFm'):
      os.remove(output + '/tempFm/' + j)

  # combine directions
  readerFw = vtk.vtkXMLImageDataReader()
  readerFw.SetFileName(output + '/categoryFw/categoryFw' + str(i) + '.vti')
  readerFw.Update()

  readerBw = vtk.vtkXMLImageDataReader()
  readerBw.SetFileName(output + '/categoryBw/categoryBw' + str(i) + '.vti')
  readerBw.Update()


  dataSet = vtk.vtkImageData()
  dataSet.CopyStructure(readerFw.GetOutput())

  ftle = np.zeros(dataSet.GetNumberOfPoints())
  strength = np.zeros(dataSet.GetNumberOfPoints())
  measure = np.zeros(dataSet.GetNumberOfPoints())
  category = np.zeros(dataSet.GetNumberOfPoints())
  v1Fw = np.zeros((dataSet.GetNumberOfPoints(), 3))
  v1Bw = np.zeros((dataSet.GetNumberOfPoints(), 3))
  v2Fw = np.zeros((dataSet.GetNumberOfPoints(), 3))
  v2Bw = np.zeros((dataSet.GetNumberOfPoints(), 3))
  if i > 0 and i < len(listdir(input))-1:

#  if i == 0
#    ftle = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("ftle"))
#    strength = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("strength"))
#    measure = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("measure"))
#    category = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("category"))
#  elif i == 20:
#    ftle = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("ftle"))
#    strength = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("strength"))
#    measure = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("measure"))
#    category = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("category"))
    strengthFw = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("strength"))
    strengthBw = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("strength"))
    strength = np.minimum(strengthFw/max(1,i), strengthBw/max(1,(len(listdir(input))-1-i)))

    measureFw = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("measure"))
    measureBw = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("measure"))
    v1Fw = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("v1"))
    v1Bw = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("v1"))
    v2Fw = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("v2"))
    v2Bw = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("v2"))
    for j in range(len(measureFw)):
      measure[j] = np.minimum(strengthFw[j]/max(1,i), strengthBw[j]/max(1,(len(listdir(input))-1-i))) * abs(np.dot(v1Fw[j],v2Bw[j]))
#      measure[j] = np.minimum(strengthFw[j]/max(1,i), strengthBw[j]/max(1,(len(listdir(input))-1-i)))

    ftleFw = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("ftle"))
    ftleBw = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("ftle"))
    ftle = np.minimum(ftleFw/max(1,i), ftleBw/max(1,(len(listdir(input))-1-i)))

    categoryFw = VN.vtk_to_numpy(readerFw.GetOutput().GetPointData().GetArray("category"))
    categoryBw = VN.vtk_to_numpy(readerBw.GetOutput().GetPointData().GetArray("category"))
    category = [0] * len(categoryBw)
    for j in range(len(categoryBw)):
      if categoryFw[j] == 1 and categoryBw[j] == -1:
        category[j] = 1
        measure[j] = 0
      elif categoryFw[j] == -1 and categoryBw[j] == 1:
        category[j] = -1
        measure[j] = 0
      elif categoryFw[j] == 0 and categoryBw[j] == 0:
        category[j] = 0
      else:
        category[j] = float('nan')
        strength[j] = 0
        measure[j] = 0

  categoryBigger = np.array(category).reshape(readerBw.GetOutput().GetExtent()[3]-readerBw.GetOutput().GetExtent()[2]+1, readerBw.GetOutput().GetExtent()[1]-readerBw.GetOutput().GetExtent()[0]+1)
  for n in range(1,3):
    categoryTemp = categoryBigger
    for j in range(n,readerBw.GetOutput().GetExtent()[3]-readerBw.GetOutput().GetExtent()[2]-n):
      for k in range(n,readerBw.GetOutput().GetExtent()[1]-readerBw.GetOutput().GetExtent()[0]-n):
        if math.isnan(categoryBigger[j,k]):
          for l in range(-n,n+1):
            for m in range(-n,n+1):
              if not math.isnan(categoryTemp[j+l,k+m]):
                categoryBigger[j,k] = categoryTemp[j+l,k+m]


  measureArray = VN.numpy_to_vtk(measure)
  measureArray.SetName("measure")
  dataSet.GetPointData().AddArray(measureArray)

  strengthArray = VN.numpy_to_vtk(strength)
  strengthArray.SetName("strength")
  dataSet.GetPointData().AddArray(strengthArray)

  v1BwArray = VN.numpy_to_vtk(v1Bw)
  v1BwArray.SetName("v1Bw")
  dataSet.GetPointData().AddArray(v1BwArray)

  v1FwArray = VN.numpy_to_vtk(v1Fw)
  v1FwArray.SetName("v1Fw")
  dataSet.GetPointData().AddArray(v1FwArray)

  v2BwArray = VN.numpy_to_vtk(v2Bw)
  v2BwArray.SetName("v2Bw")
  dataSet.GetPointData().AddArray(v2BwArray)

  v2FwArray = VN.numpy_to_vtk(v2Fw)
  v2FwArray.SetName("v2Fw")
  dataSet.GetPointData().AddArray(v2FwArray)

  ftleArray = VN.numpy_to_vtk(ftle)
  ftleArray.SetName("ftle")
  dataSet.GetPointData().AddArray(ftleArray)

  categoryArray = VN.numpy_to_vtk(category)
  categoryArray.SetName("category")
  dataSet.GetPointData().AddArray(categoryArray)

  categoryBiggerArray = VN.numpy_to_vtk(categoryBigger.reshape(dataSet.GetNumberOfPoints()))
  categoryBiggerArray.SetName("categoryBigger")
  dataSet.GetPointData().AddArray(categoryBiggerArray)

  writer = vtk.vtkXMLImageDataWriter()
  writer.SetInputData(dataSet)
  writer.SetFileName(output + '/category/category' + str(i) + '.vti')
  writer.Write()

submitCommand = 'vtkpython ' + os.path.join(scriptPath, 'convertToSpaceTime.py') + ' ' + output + '/category ' + output + '/category'
print ('-submitting: ' + submitCommand)
os.system(submitCommand)

submitCommand = 'vtkpython ' + os.path.join(scriptPath, 'saddleCoreSpacetime.py') + ' ' + output + '/category/spaceTimeConverted.vti ' + input + '/spaceTimeConverted.vti ' + output + '/separatrix'
print ('-submitting: ' + submitCommand)
os.system(submitCommand)
