# this script advects a scalar field along a given flowmap
# it is for our envirVis paper

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import eig
import vtk.util.numpy_support as VN
import re

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)

def listdir(path):
  return [f for f in os.listdir(path) if not f.startswith('.')]
#-----------------------------------------------------------------------------------
# paramters


#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 4:
  print ("use: advectScalar.py <filepath to file that contains the imagedata on which the flowmap lives> <filepath to folder that contains the flowmap> <filepath to folder that will contain the output>")
  sys.exit()

imageDataInput = sys.argv[1]

input = sys.argv[2]
if not os.path.isdir(input):
  print ("enter filepath to folder that contains the fowmap")
  sys.exit()
#print sorted(os.listdir(input))

output = sys.argv[3]
if not os.path.isdir(output):
  os.makedirs(output)


#-----------------------------------------------------------------------------------
# main program
reader = vtk.vtkXMLImageDataReader()
reader.SetFileName(imageDataInput)
reader.Update()
startData = reader.GetOutput()
#print startData.GetPointData()

fileList = sorted_aphanumeric(listdir(input))
for i in range(len(fileList)):
#  print i
  filename, file_extension = os.path.splitext(fileList[i])

  reader = vtk.vtkDataSetReader()
  reader.SetFileName(input + '/' + fileList[i])
  reader.Update()
#  print reader.GetOutput()

  delaunay = vtk.vtkDelaunay2D()
  delaunay.SetInputData(reader.GetOutput())
  delaunay.Update()
  data = delaunay.GetOutput()

  dataSet = vtk.vtkPolyData()
  dataSet.CopyStructure(data)

  for j in range(startData.GetPointData().GetNumberOfArrays()):
#    print startData.GetPointData().GetArray(j).GetName(), startData.GetPointData().GetArray(j).GetNumberOfComponents()
    if startData.GetPointData().GetArray(j).GetNumberOfComponents() > 1:
      continue
    array = vtk.vtkDoubleArray()
    array.SetNumberOfTuples(dataSet.GetNumberOfPoints())
    array.SetName(startData.GetPointData().GetArray(j).GetName())
    dataSet.GetPointData().AddArray(array)

    # find out old index and store scalar value
    oldIds = [-1] * startData.GetNumberOfPoints()
    newIds = [-1] * startData.GetNumberOfPoints()
    for k in range(data.GetNumberOfPoints()):
      index = int(data.GetPointData().GetArray("ParticleId").GetTuple1(k))
      array.SetTuple1(k, startData.GetPointData().GetArray(j).GetTuple1(index))

  dataSet.GetPointData().RemoveArray("vtkValidPointMask")
  
  writer = vtk.vtkDataSetWriter()
  writer.SetInputData(dataSet)
  writer.SetFileName(output + '/' + fileList[i])
  writer.Write()

  probe = vtk.vtkProbeFilter()
  probe.SetInputData(startData)
  probe.SetSourceData(dataSet)
  probe.Update()

  writer = vtk.vtkXMLImageDataWriter()
  writer.SetInputData(probe.GetOutput())
  writer.SetFileName(output + '/advected' + str(i) + '.vti')
  writer.Write()
