# this script converts a dataset from our test suite into space time for tobias algorithm, i.e. 2D vector plus time in z-direction

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import eig
import vtk.util.numpy_support as VN
import re

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)

def listdir(path):
  return [f for f in os.listdir(path) if not f.startswith('.') and f.find('spaceTime') == -1 and os.path.splitext(f)[1] == '.vti']
#-----------------------------------------------------------------------------------
# paramters
d = 3
print ("length of a vector =", d)


#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 2:
  print ("use: convertToSpaceTime.py <filepath to folder that contains the data> <optional: filepath to folder that will contain the output>")
  sys.exit()

input = sys.argv[1]
if not os.path.isdir(input):
  print ("enter filepath to folder that contains the data as first argument")
  sys.exit()
#print sorted(os.listdir(input))

if len(sys.argv) == 3:
  output = sys.argv[2]
else:
  output = input
if not os.path.isdir(output):
  os.makedirs(output)


#-----------------------------------------------------------------------------------
# main program
timesteps = sorted_aphanumeric(listdir(input))
#print timesteps

reader = vtk.vtkXMLImageDataReader()
reader.SetFileName(input + '/' + timesteps[0])
reader.Update()
data = reader.GetOutput()

spaceTimeData = vtk.vtkImageData()
spaceTimeData.CopyStructure(data)
spaceTimeData.SetExtent(data.GetExtent()[0], data.GetExtent()[1], data.GetExtent()[2], data.GetExtent()[3], 0, len(timesteps)-1)
spaceTimeData.SetSpacing(data.GetSpacing()[0], data.GetSpacing()[1], 1)

for j in range(data.GetPointData().GetNumberOfArrays()):
  numberOfComponents = data.GetPointData().GetArray(j).GetNumberOfComponents()
  spaceTime = np.zeros((len(timesteps), data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1, numberOfComponents))

  for i in range(len(timesteps)):
    #for i in range(2):
    reader = vtk.vtkXMLImageDataReader()
    reader.SetFileName(input + '/' + timesteps[i])
    reader.Update()
    data = reader.GetOutput()
    array = VN.vtk_to_numpy(data.GetPointData().GetArray(j)).reshape(data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1, numberOfComponents)
    spaceTime[i,:,:,:] = array[:,:,:]

  if numberOfComponents == 3:
    spaceTime[:,:,:,2] = np.full((len(timesteps), data.GetExtent()[3]-data.GetExtent()[2]+1, data.GetExtent()[1]-data.GetExtent()[0]+1),1)

  spaceTimeArray = VN.numpy_to_vtk(spaceTime.reshape(data.GetNumberOfPoints()*len(timesteps),numberOfComponents))
  spaceTimeArray.SetName(data.GetPointData().GetArray(j).GetName())
  spaceTimeData.GetPointData().AddArray(spaceTimeArray)

  if numberOfComponents == 3:
    spaceTimeData.GetPointData().SetActiveVectors(spaceTimeArray.GetName())
#  if numberOfComponents == 1:
#    spaceTimeData.GetPointData().SetScalars(spaceTimeArray)

writer = vtk.vtkXMLImageDataWriter()
writer.SetInputData(spaceTimeData)
writer.SetFileName(output + '/spaceTimeConverted.vti' )
writer.Write()
