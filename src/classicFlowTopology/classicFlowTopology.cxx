// VTK includes
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkImageData.h>
#include "vtkUnstructuredGrid.h"
#include <vtkPoints.h>
#include <vtkQuad.h>
#include <vtkTriangle.h>
#include <vtkCellArray.h>
#include <vtkDoubleArray.h>
#include <vtkVertex.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkDataSetTriangleFilter.h>
#include <vtkGradientFilter.h>
#include <vtkProbeFilter.h>
#include <vtkXMLImageDataReader.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkXMLMultiBlockDataWriter.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkNew.h>
#include <vtkSmartPointer.h>
#include <vtkVector.h>
#include <vtkMatrix3x3.h>
#include <vtkStreamTracer.h>
#include <vtkRegularPolygonSource.h>
#include <vtkRuledSurfaceFilter.h>
#include <vtkIntersectionPolyDataFilter.h>
#include <vtkAppendPolyData.h>
#include <vtkMath.h>
#include <cmath>
// Eigen3
#include <Eigen/Eigenvalues>

// C/C++ includes
// #include <stdlib.h>
// #include <stdio.h>
// #include <sys/stat.h>
#include <map>

#define epsilon ( 1e-10 )

//----------------------------------------------------------------------------------
vtkIdType getArrayIndex(std::vector<int> coord, std::vector<int> dimensions, int* extent)
{
  return (coord[0] - extent[0]) + (coord[1] - extent[2]) * dimensions[0] + (coord[2] - extent[4]) * dimensions[0] * dimensions[1];
}

//----------------------------------------------------------------------------------
std::vector<int> getCoord(vtkIdType index, std::vector<int> dimensions, int* extent)
{
  int z = 0;
  if (dimensions[2] > 1)
  {
    z = index / dimensions[0] / dimensions[1];
  }

  int y = (index - z * dimensions[0] * dimensions[1]) / dimensions[0];

  std::vector<int> arr(3);
  arr[0] = index - z * dimensions[0] * dimensions[1] - y * dimensions[0] + extent[0];
  arr[1] = y + extent[2];
  arr[2] = z + extent[4];

  return arr;
}

//------------------------------------------------------------------------------------------
vtkSmartPointer<vtkImageData> padField(vtkImageData* field,
                                       std::string nameOfPointData)
{
  int dim = field->GetDataDimension();
  int extent[6];
  field->GetExtent(extent);

  int dataExtentPad[6] = {0, 0, 0, 0, 0, 0};
  for (int i = 0; i < dim; i++)
  {
    dataExtentPad[2*i] = extent[2*i]-1;
    dataExtentPad[2*i+1] = extent[2*i+1]+1;
  }

  vtkNew<vtkImageData> output;
  output->SetOrigin(field->GetOrigin());
  output->SetSpacing(field->GetSpacing());
  output->SetExtent(dataExtentPad);

  vtkDataArray* origArray = field->GetPointData()->GetArray(nameOfPointData.c_str());

  vtkNew<vtkDoubleArray> paddedArray;
  paddedArray->SetName(nameOfPointData.c_str());
  paddedArray->SetNumberOfComponents(origArray->GetNumberOfComponents());
  paddedArray->SetNumberOfTuples(output->GetNumberOfPoints());
  paddedArray->Fill(0.0);

  const int* tmp = field->GetDimensions();
  std::vector<int> origSize = std::vector<int>(tmp, tmp + 3);
  
  tmp = output->GetDimensions();
  std::vector<int> paddedSize = std::vector<int>(tmp, tmp + 3);

  /* padding field for edge critical points detection */
  for (vtkIdType i = 0; i < output->GetNumberOfPoints(); i++)
  {
    std::vector<int> val_coord = getCoord(i, paddedSize, dataExtentPad);
    double val_mult[3] = { 1.0, 1.0, 1.0 };
    for (int d = 0; d < dim; d++)
    {
      if (val_coord[d] == dataExtentPad[2*d])
      {
        val_coord[d] += 2;
        val_mult[d] = -1.0;
      }
      else if (val_coord[d] == dataExtentPad[2*d+1])
      {
        val_coord[d] -= 2;
        val_mult[d] = -1.0;
      }
      else if (val_coord[d] == dataExtentPad[2*d]+1)
      {
        val_mult[d] = 0.0;
      }
      else if (val_coord[d] == dataExtentPad[2*d+1]-1)
      {
        val_mult[d] = 0.0;
      }
    }

    vtkIdType val_index = getArrayIndex(val_coord, origSize, extent);
    double* value = origArray->GetTuple(val_index);
    double val_res[3] = { val_mult[0]*value[0], val_mult[1]*value[1], val_mult[2]*value[2] };
    paddedArray->SetTuple(i, val_res);
  }

  output->GetPointData()->SetVectors(paddedArray);
  output->GetPointData()->SetActiveVectors(nameOfPointData.c_str());

  return output.GetPointer();
}

int computeCriticalPoints3D (vtkSmartPointer<vtkPolyData> criticalPoints, vtkSmartPointer<vtkUnstructuredGrid> tridataset)
{
  for (int cellId = 0; cellId < tridataset->GetNumberOfCells(); cellId++)
  {
    auto cell = tridataset->GetCell(cellId);
    vtkIdType indices[4] = {cell->GetPointId(0), cell->GetPointId(1), cell->GetPointId(2), cell->GetPointId(3)};
    // std::cout << "Indices: " << indices[0] << ", " << indices[1] << ", " << indices[2] << ", " << indices[3] << std::endl;

    vtkVector3d coords[4] = {vtkVector3d(tridataset->GetPoint(indices[0])),
      vtkVector3d(tridataset->GetPoint(indices[1])),
      vtkVector3d(tridataset->GetPoint(indices[2])),
      vtkVector3d(tridataset->GetPoint(indices[3]))};

    vtkVector3d values[4] = {vtkVector3d(tridataset->GetPointData()->GetVectors()->GetTuple(indices[0])),
      vtkVector3d(tridataset->GetPointData()->GetVectors()->GetTuple(indices[1])),
      vtkVector3d(tridataset->GetPointData()->GetVectors()->GetTuple(indices[2])),
      vtkVector3d(tridataset->GetPointData()->GetVectors()->GetTuple(indices[3]))};

    // std::cout << "values[0]: " << values[0][0] << ", " << values[0][1] << ", " << values[0][2] << ", " << values[0][3] << std::endl;

    vtkNew<vtkMatrix3x3> valueMatrix;
    // valueMatrix->Zero();
    for (int i = 0; i < 3; i++)
    {
      for (int j = 0; j < 3; j++)
      {
        valueMatrix->SetElement( j, i, values[3][j] - values[i][j] );
      }
    }
    // valueMatrix->Print(std::cout);

    valueMatrix->Invert();
    // valueMatrix->PrintSelf(std::cout, vtkIndent(2));
    double zeroBase[3] = {values[3][0], values[3][1], values[3][2]};
    valueMatrix->MultiplyPoint(zeroBase, zeroBase);

    double zeroPos[3] = { coords[0][0] * zeroBase[0] + coords[1][0] * zeroBase[1] + coords[2][0] * zeroBase[2] + coords[3][0] * (1.0-zeroBase[0]-zeroBase[1]-zeroBase[2]),
      coords[0][1] * zeroBase[0] + coords[1][1] * zeroBase[1] + coords[2][1] * zeroBase[2] + coords[3][1] * (1.0-zeroBase[0]-zeroBase[1]-zeroBase[2]),
      coords[0][2] * zeroBase[0] + coords[1][2] * zeroBase[1] + coords[2][2] * zeroBase[2] + coords[3][2] * (1.0-zeroBase[0]-zeroBase[1]-zeroBase[2]) };

    // Check if zeroPos is inside the cell
    if (zeroBase[0] >= -epsilon && zeroBase[1] >= -epsilon && zeroBase[2] >= -epsilon && zeroBase[0]+zeroBase[1]+zeroBase[2] <= 1.0+epsilon)
    {
//       std::cout <<cellId<< " zeroBase: " << zeroBase[0] << ", " << zeroBase[1] << ", " << zeroBase[2] << std::endl;
//      std::cout <<cellId<< " zeroPos: " << zeroPos[0] << ", " << zeroPos[1] << ", " << zeroPos[2] << std::endl;
      bool isNewPoint = 1;
      for (int i = 0; i < criticalPoints->GetNumberOfPoints(); ++i)
      {
        if (sqrt(vtkMath::Distance2BetweenPoints(zeroPos, criticalPoints->GetPoint(i))) < 1e-5)
        {
          isNewPoint = 0;
        }
      }
      if (isNewPoint)
      {
//        std::cout <<cellId<< " zeroPos: " << zeroPos[0] << ", " << zeroPos[1] << ", " << zeroPos[2] << std::endl;
        criticalPoints->GetPoints()->InsertNextPoint(zeroPos);
        vtkNew<vtkVertex> vertex;
        vertex->GetPointIds()->SetId(0, criticalPoints->GetNumberOfPoints()-1);
        criticalPoints->GetVerts()->InsertNextCell(vertex);
      }
    }
  }
  return EXIT_SUCCESS;
}

int computeCriticalPoints2D (vtkSmartPointer<vtkPolyData> criticalPoints, vtkSmartPointer<vtkUnstructuredGrid> tridataset)
{
  std::map<std::string, int> criticalPtsMap;
  int criticalPointCounter = 0;
  for (int cellId = 0; cellId < tridataset->GetNumberOfCells(); cellId++)
  {
    auto cell = tridataset->GetCell(cellId);
    vtkIdType indices[3] = {cell->GetPointId(0), cell->GetPointId(1), cell->GetPointId(2)};
    // std::cout << "Indices: " << indices[0] << ", " << indices[1] << ", " << indices[2] << std::endl;

    vtkVector3d coords[3] = {vtkVector3d(tridataset->GetPoint(indices[0])),
      vtkVector3d(tridataset->GetPoint(indices[1])),
      vtkVector3d(tridataset->GetPoint(indices[2]))};

    vtkVector3d values[3] = {vtkVector3d(tridataset->GetPointData()->GetVectors()->GetTuple(indices[0])),
      vtkVector3d(tridataset->GetPointData()->GetVectors()->GetTuple(indices[1])),
      vtkVector3d(tridataset->GetPointData()->GetVectors()->GetTuple(indices[2]))};
    // std::cout << "values[0]: " << values[0][0] << ", " << values[0][1] << ", " << values[0][2] << std::endl;

    vtkNew<vtkMatrix3x3> valueMatrix;
    // valueMatrix->Zero();
    for (int i = 0; i < 2; i++)
    {
      for (int j = 0; j < 2; j++)
      {
        valueMatrix->SetElement( j, i, values[i+1][j] - values[0][j] );
      }
    }
    // valueMatrix->Print(std::cout);

    valueMatrix->Invert();
    // valueMatrix->PrintSelf(std::cout, vtkIndent(2));
    double zeroBase[3] = {-values[0][0], -values[0][1], -values[0][2]};
    valueMatrix->MultiplyPoint(zeroBase, zeroBase);

    double zeroPos[3] = {coords[0][0] + zeroBase[0] * (coords[1][0] - coords[0][0]) + zeroBase[1] * (coords[2][0] - coords[0][0]),
      coords[0][1] + zeroBase[0] * (coords[1][1] - coords[0][1]) + zeroBase[1] * (coords[2][1] - coords[0][1]),
      coords[0][2] + zeroBase[0] * (coords[1][2] - coords[0][2]) + zeroBase[1] * (coords[2][2] - coords[0][2])};

    // Check if zeroPos is inside the cell
    if (zeroBase[0] >= -epsilon && zeroBase[1] >= -epsilon && zeroBase[0]+zeroBase[1] <= 1.0+epsilon)
    {
      std::string zeroPos_str = "("+std::to_string(zeroPos[0])+","+std::to_string(zeroPos[1])+","+std::to_string(zeroPos[2])+")";
      if (criticalPtsMap.find(zeroPos_str) == criticalPtsMap.end())
      {
        criticalPtsMap.insert(std::pair<std::string, int>(zeroPos_str, 0));
        criticalPoints->GetPoints()->InsertNextPoint(zeroPos);
        vtkNew<vtkVertex> vertex;
        vertex->GetPointIds()->SetId(0, criticalPointCounter);
        criticalPoints->GetVerts()->InsertNextCell(vertex);
        criticalPointCounter++;
      }
    }
  }
  return EXIT_SUCCESS;
}

int classify2D(int countReal, int countComplex, int countPos, int countNeg)
{
//  if (countReal == 2)
//  {
//    if (countPos == 1 && countNeg == 1)
//    {
//      // saddles
//      critType = 0;
//    }
//    else if (countPos == 2)
//    {
//      // node sources
//      critType = 4;
//    }
//    else if (countNeg == 2)
//    {
//
//      // node sinks
//      critType = 1;
//    }
//  }
//  else if (countComplex == 2)
//  {
//    if (countPos == 2)
//    {
//      // focus sources
//      critType = 3;
//    }
//    else if (countNeg == 2)
//    {
//      // focuse sinks
//      critType = 2;
//    }
//    else
//    {
//      // centers
//      critType = 5;
//    }
//  }
  //    cout<<"critType "<<critType<<endl;
  
  // make simple type that corresponds to the number of positive eigenvalues
  // source 2, saddle 1, sink 0, (center 3)
  // in analogy to ttk, where the type corresponds to the down directions
  int critType = -1;
  if (countPos + countNeg == 2)
  {
    critType = countPos;
  }
  if (countComplex == 2)
  {
    critType = 3;
  }
  return critType;
}

int classify3D(int countReal, int countComplex, int countPos, int countNeg)
{
//  if (countReal == 3)
//  {
//    if ((countPos == 2 && countNeg == 1) || (countPos == 1 && countNeg == 2))
//    {
//      // saddles
//      critType = 0;
//      // cout << "Node Saddle: " << endl;
//      // cout << eigenMatrix << endl;
//      // cout << "The first eigenvalue of the 3x3 matrix is:" << endl << eigenS.eigenvalues().row(0) << endl;
//      // cout << "The second eigenvalue of the 3x3 matrix is:" << endl << eigenS.eigenvalues().row(1) << endl;
//      // cout << "The third eigenvalue of the 3x3 matrix is:" << endl << eigenS.eigenvalues().row(2) << endl;
//
//      // cout << "The first eigenvector of the 3x3 matrix is:" << endl << eigenS.eigenvectors().col(0) << endl;
//      // cout << "The second eigenvector of the 3x3 matrix is:" << endl << eigenS.eigenvectors().col(1) << endl;
//      // cout << "The third eigenvector of the 3x3 matrix is:" << endl << eigenS.eigenvectors().col(2) << endl;
//    }
//    else if (countPos == 3)
//    {
//      // sources
//      critType = 4;
//    }
//    else if (countNeg == 3)
//    {
//
//      // sinks
//      critType = 1;
//    }
//  }
//  else if (countReal == 1 && countComplex == 2)
//  {
//    if (countPos == 3)
//    {
//      // spiral sources
//      critType = 3;
//    }
//    else if (countNeg == 3)
//    {
//      // spiral sinks
//      critType = 2;
//    }
//    else if ((countPos == 2 && countNeg == 1) || (countPos == 1 && countNeg == 2))
//    {
//      // spiral saddles
//      critType = 5;
//      // cout << "Spiral Saddle: " << endl;
//      // cout << eigenMatrix << endl;
//      // cout << "The first eigenvalue of the 3x3 matrix is:" << endl << eigenS.eigenvalues().row(0) << endl;
//      // cout << "The second eigenvalue of the 3x3 matrix is:" << endl << eigenS.eigenvalues().row(1) << endl;
//      // cout << "The third eigenvalue of the 3x3 matrix is:" << endl << eigenS.eigenvalues().row(2) << endl;
//
//      // cout << "The first eigenvector of the 3x3 matrix is:" << endl << eigenS.eigenvectors().col(0) << endl;
//      // cout << "The second eigenvector of the 3x3 matrix is:" << endl << eigenS.eigenvectors().col(1) << endl;
//      // cout << "The third eigenvector of the 3x3 matrix is:" << endl << eigenS.eigenvectors().col(2) << endl;
//    }
//  }

  // make simple type that corresponds to the number of positive eigenvalues
  // source 3, saddle 2 or 1, sink 0, (center 4)
  // in analogy to ttk, where the type corresponds to the down directions
  int critType = -1;
  if (countComplex > 0)
  {
    critType = 4;
  }
  if (countPos + countNeg == 3)
  {
    critType = countPos;
  }
  return critType;
}

int mapToBoundary(double(& point)[3], vtkSmartPointer<vtkImageData> dataset)
{
  double bounds[6];
  dataset->GetBounds(bounds);
  int closestBound = 0;
  for (int j = 0; j < 6; j++)
  {
    if (abs(point[j/2] - bounds[j]) < abs(point[closestBound/2]-bounds[closestBound]))
    {
      closestBound = j;
    }
  }
  point[closestBound/2] = bounds[closestBound];
  return EXIT_SUCCESS;
}

int surface (bool isBackward, double normal[3], double zeroPos[3], vtkSmartPointer<vtkPolyData> streamSurfaces,  vtkSmartPointer<vtkImageData> dataset, double dist, int maxNumSteps)
{
//  cout<<"surface "<<isBackward<<endl;
  // generate circle and add first point again in the back to avoid gap
  vtkNew<vtkRegularPolygonSource> circle;
  circle->GeneratePolygonOff();
  circle->SetNumberOfSides(6);
  circle->SetRadius(dist);
  circle->SetCenter(zeroPos);
  circle->SetNormal(normal);
  circle->Update();
  // close circle exactly with a point instead of an edge to correctly treat points exiting the boundary
  circle->GetOutput()->GetPoints()->InsertNextPoint(circle->GetOutput()->GetPoint(0));
  vtkNew<vtkPolyData> currentCircle;
  currentCircle->SetPoints(circle->GetOutput()->GetPoints());
  vtkNew<vtkDoubleArray> integrationTimeArray;
  integrationTimeArray->SetName("IntegrationTime");
  currentCircle->GetPointData()->AddArray(integrationTimeArray);
  for (int i = 0; i < currentCircle->GetNumberOfPoints(); ++i)
  {
    integrationTimeArray->InsertNextTuple1(0);
  }
////  this is for comparison with the standard ruled surface
////  vtkNew<vtkXMLPolyDataWriter> polyWriter;
////  polyWriter->SetInputData(currentCircle);
////  polyWriter->SetFileName("/Users/bujack/Documents/flowTopology/sandbox/data/classicFlowTopology/3DsaddleAT6/classicFlowTopology/circle.vtp");
////  polyWriter->Write();
//
//  vtkNew<vtkStreamTracer> streamTracer;
//  streamTracer->SetInputData(dataset);
//  streamTracer->SetIntegratorTypeToRungeKutta4();
//  streamTracer->SetIntegrationStepUnit(1);
//  streamTracer->SetInitialIntegrationStep(dist);
//  streamTracer->SetIntegrationDirection(isBackward);
//  streamTracer->SetComputeVorticity(0);
//  streamTracer->SetMaximumNumberOfSteps(maxNumSteps);
//  streamTracer->SetSourceData(currentCircle);
//  streamTracer->SetMaximumPropagation(dist*maxNumSteps);
//  streamTracer->Update();
//
////  polyWriter->SetInputData(streamTracer->GetOutput());
////  polyWriter->SetFileName("/Users/bujack/Documents/flowTopology/sandbox/data/classicFlowTopology/3DsaddleAT6/classicFlowTopology/lines.vtp");
////  polyWriter->Write();
//
//  vtkNew<vtkRuledSurfaceFilter> vtkRuledSurface;
//  vtkRuledSurface->SetInputData(streamTracer->GetOutput());
//  vtkRuledSurface->Update();
//  vtkRuledSurface->SetRuledModeToResample();
//  vtkRuledSurface->SetResolution(maxNumSteps, 1);
//  vtkRuledSurface->Update();
//
//  vtkNew<vtkAppendPolyData> appendSurfaces;
//  appendSurfaces->AddInputData(vtkRuledSurface->GetOutput());
//  appendSurfaces->AddInputData(streamSurfaces);
//  appendSurfaces->Update();
//  streamSurfaces->DeepCopy(appendSurfaces->GetOutput());
//
//  return EXIT_SUCCESS;
//
////  polyWriter->SetInputData(vtkRuledSurface->GetOutput());
////  polyWriter->SetFileName("/Users/bujack/Documents/flowTopology/sandbox/data/classicFlowTopology/3DsaddleAT/classicFlowTopology/surface.vtp");
////  polyWriter->Write();

//  double integrationTime = 0;
  for (int m = 0; m < maxNumSteps; m++)
  {
//    cout<<"iteration = "<<m<<" "<<currentCircle->GetNumberOfPoints()<<endl;
    // advect currentCircle
    // the output will be ordered: 0, advect(0), 1, advect(1), 2...
    // but if a point reaches the boundary, its advected point is just missing
    vtkNew<vtkStreamTracer> streamTracerStep;
    streamTracerStep->SetInputData(dataset);
    streamTracerStep->SetIntegratorTypeToRungeKutta4();
    streamTracerStep->SetIntegrationStepUnit(1);
    streamTracerStep->SetInitialIntegrationStep(dist);
    streamTracerStep->SetIntegrationDirection(isBackward);
    streamTracerStep->SetComputeVorticity(0);
    streamTracerStep->SetMaximumNumberOfSteps(0);
    streamTracerStep->SetSourceData(currentCircle);
    streamTracerStep->Update();
    //      cout<<streamTracerStep->GetOutput()->GetNumberOfPoints()<<endl;

    // fill in points that were not advected because they reached the boundary
    // i.e. copy a point k with integrationtime(k)==0 if its successor also has integrationtime(k+1)==0
    vtkNew<vtkPolyData> orderedSurface;
    vtkNew<vtkPoints> orderedSurfacePoints;
    vtkNew<vtkCellArray> orderedSurfaceCells;
    orderedSurface->SetPoints(orderedSurfacePoints);
    orderedSurface->SetPolys(orderedSurfaceCells);

    vtkNew<vtkDoubleArray> integrationTimeArray;
    integrationTimeArray->SetName("IntegrationTime");
    orderedSurface->GetPointData()->AddArray(integrationTimeArray);

    int cirrentCircleIndex = -1;
    for (int k = 0; k < streamTracerStep->GetOutput()->GetNumberOfPoints() - 1; k++)
    {
      if (streamTracerStep->GetOutput()->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k) == 0)
      {
        cirrentCircleIndex++;
      }
//      cout<<k<<" "<<cirrentCircleIndex<<endl;
      orderedSurfacePoints->InsertNextPoint(streamTracerStep->GetOutput()->GetPoint(k));
      integrationTimeArray->InsertNextTuple1(streamTracerStep->GetOutput()->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k) + currentCircle->GetPointData()->GetArray("IntegrationTime")->GetTuple1(cirrentCircleIndex));

      if (streamTracerStep->GetOutput()->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k) == 0)
      {
        if (streamTracerStep->GetOutput()->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+1) == 0)
        {
//          double point[3];
//          streamTracerStep->GetOutput()->GetPoint(k, point);
//          mapToBoundary(point, dataset);
//          orderedSurfacePoints->InsertNextPoint(point);
          orderedSurfacePoints->InsertNextPoint(streamTracerStep->GetOutput()->GetPoint(k));
          integrationTimeArray->InsertNextTuple1(currentCircle->GetPointData()->GetArray("IntegrationTime")->GetTuple1(cirrentCircleIndex));
        }
      }
    }

//    cout<<streamTracerStep->GetOutput()->GetNumberOfPoints() - 1<<" "<<cirrentCircleIndex<<endl;
    orderedSurfacePoints->InsertNextPoint(streamTracerStep->GetOutput()->GetPoint(streamTracerStep->GetOutput()->GetNumberOfPoints() - 1));
    integrationTimeArray->InsertNextTuple1(streamTracerStep->GetOutput()->GetPointData()->GetArray("IntegrationTime")->GetTuple1(streamTracerStep->GetOutput()->GetNumberOfPoints() - 1) + currentCircle->GetPointData()->GetArray("IntegrationTime")->GetTuple1(cirrentCircleIndex));
    if (streamTracerStep->GetOutput()->GetPointData()->GetArray("IntegrationTime")->GetTuple1(streamTracerStep->GetOutput()->GetNumberOfPoints() - 1) == 0)
    {
      orderedSurfacePoints->InsertNextPoint(streamTracerStep->GetOutput()->GetPoint(streamTracerStep->GetOutput()->GetNumberOfPoints() - 1));
      integrationTimeArray->InsertNextTuple1(currentCircle->GetPointData()->GetArray("IntegrationTime")->GetTuple1(cirrentCircleIndex));
    }

    // add arrays
    vtkNew<vtkDoubleArray> iterationArray;
    iterationArray->SetName("iteration");
    iterationArray->SetNumberOfTuples(orderedSurface->GetNumberOfPoints());
    orderedSurface->GetPointData()->AddArray(iterationArray);

    vtkNew<vtkDoubleArray> indexArray;
    indexArray->SetName("index");
    indexArray->SetNumberOfTuples(orderedSurface->GetNumberOfPoints());
    orderedSurface->GetPointData()->AddArray(indexArray);
    for (int k = 0; k < orderedSurface->GetNumberOfPoints(); k++)
    {
      indexArray->SetTuple1(k,k);
      iterationArray->SetTuple1(k,m);
    }

    // insert cells
    for (int k = 0; k < orderedSurface->GetNumberOfPoints() - 2; k+=2)
    {
//      cout<<m<<" "<<k<<" "<<orderedSurface->GetNumberOfPoints()<<endl;
//      vtkNew<vtkQuad> quad;
//      quad->GetPointIds()->SetId(0, k);
//      quad->GetPointIds()->SetId(1, k+2);
//      quad->GetPointIds()->SetId(2, k+3);
//      quad->GetPointIds()->SetId(3, k+1);
//      orderedSurfaceCells->InsertNextCell(quad);

      if (abs(orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+1) - orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k) ) > 1e-10 && abs(orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+3) - orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+2)) > 1e-10)
      {
        double p0[3];
        orderedSurface->GetPoint(k, p0);
        double p1[3];
        orderedSurface->GetPoint(k+1, p1);
        double p2[3];
        orderedSurface->GetPoint(k+2, p2);
        double p3[3];
        orderedSurface->GetPoint(k+3, p3);
        vtkNew<vtkTriangle> triangle1;
        vtkNew<vtkTriangle> triangle2;

        if (sqrt(vtkMath::Distance2BetweenPoints(p0, p3)) > sqrt(vtkMath::Distance2BetweenPoints(p1, p2)))
        {
        triangle1->GetPointIds()->SetId(0, k);
        triangle1->GetPointIds()->SetId(1, k+1);
        triangle1->GetPointIds()->SetId(2, k+2);

        triangle2->GetPointIds()->SetId(0, k+1);
        triangle2->GetPointIds()->SetId(1, k+3);
        triangle2->GetPointIds()->SetId(2, k+2);
        }
        else
        {
          triangle1->GetPointIds()->SetId(0, k);
          triangle1->GetPointIds()->SetId(1, k+3);
          triangle1->GetPointIds()->SetId(2, k+2);

          triangle2->GetPointIds()->SetId(0, k);
          triangle2->GetPointIds()->SetId(1, k+1);
          triangle2->GetPointIds()->SetId(2, k+3);
        }
        orderedSurfaceCells->InsertNextCell(triangle1);
        orderedSurfaceCells->InsertNextCell(triangle2);
      }
//      else if (abs(orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+1) - integrationTime ) > 1e-10)
//      {
//        vtkNew<vtkTriangle> triangle1;
//        triangle1->GetPointIds()->SetId(0, k);
//        triangle1->GetPointIds()->SetId(1, k+1);
//        triangle1->GetPointIds()->SetId(2, k+2);
//        orderedSurfaceCells->InsertNextCell(triangle1);
//      }
//      else
//      {
//        vtkNew<vtkTriangle> triangle1;
//        triangle1->GetPointIds()->SetId(0, k);
//        triangle1->GetPointIds()->SetId(1, k+3);
//        triangle1->GetPointIds()->SetId(2, k+2);
//        orderedSurfaceCells->InsertNextCell(triangle1);
//      }
    }

    // adaptively insert new points where neighbors have diverged
    vtkNew<vtkPoints> newCirclePoints;
    currentCircle->SetPoints(newCirclePoints);
    vtkNew<vtkDoubleArray> newIntegrationTimeArray;
    newIntegrationTimeArray->SetName("IntegrationTime");
    currentCircle->GetPointData()->RemoveArray("IntegrationTime");
    currentCircle->GetPointData()->AddArray(newIntegrationTimeArray);
    for (int k = 0; k < orderedSurface->GetNumberOfPoints()-2; k+=2)
    {
      newCirclePoints->InsertNextPoint(orderedSurface->GetPoint(k+1));
      newIntegrationTimeArray->InsertNextTuple1(orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+1));
      
      double p0[3];
      orderedSurface->GetPoint(k+1, p0);
      double p1[3];
      orderedSurface->GetPoint(k+3, p1);

      if (sqrt(vtkMath::Distance2BetweenPoints(p0, p1)) > dist && abs(orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+1) - orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k)) > 1e-10 && abs(orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+3) - orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+2)) > 1e-10)
      {
        newCirclePoints->InsertNextPoint((p0[0]+p1[0])/2, (p0[1]+p1[1])/2, (p0[2]+p1[2])/2);
        newIntegrationTimeArray->InsertNextTuple1((orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+1)+orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(k+3))/2);
      }
    }
    newCirclePoints->InsertNextPoint(orderedSurface->GetPoint(orderedSurface->GetNumberOfPoints()-1));
    newIntegrationTimeArray->InsertNextTuple1(orderedSurface->GetPointData()->GetArray("IntegrationTime")->GetTuple1(orderedSurface->GetNumberOfPoints()-1));

//    integrationTime += streamTracerStep->GetOutput()->GetPointData()->GetArray("IntegrationTime")->GetRange()[1-isBackward];
//    //    cout<<m<<" "<<integrationTime<<endl

    // add current surface strip to the so far computed stream surface
    vtkNew<vtkAppendPolyData> appendSurfaces;
    appendSurfaces->AddInputData(orderedSurface);
    appendSurfaces->AddInputData(streamSurfaces);
    appendSurfaces->Update();
    streamSurfaces->DeepCopy(appendSurfaces->GetOutput());
    //    cout<<m<<" "<<streamSurfaces->GetNumberOfPoints()<<" "<<orderedSurface->GetNumberOfPoints()<<" "<<currentCircle->GetNumberOfPoints()<<endl;

    // stop criterion if all points have left the boundary
    if (streamTracerStep->GetOutput()->GetPointData()->GetArray("IntegrationTime")->GetRange()[1-isBackward] == 0)
    {
      cout<<m<<" surface stagnates at IntegrationTime "<<currentCircle->GetPointData()->GetArray("IntegrationTime")->GetRange()[1-isBackward]<<endl;
      break;
    }
    if (currentCircle == nullptr)
    {
      cout<<"circle is empty."<<endl;
      break;
    }
  }
  return EXIT_SUCCESS;
}

int computeSeparatrices (vtkSmartPointer<vtkPolyData> criticalPoints, vtkSmartPointer<vtkPolyData> separatrices, vtkSmartPointer<vtkPolyData> surfaces,  vtkSmartPointer<vtkImageData> dataset, vtkSmartPointer<vtkImageData> graddataset, double dist, int maxNumSteps)
{
  // Compute eigenvectors & eigenvalues
//    cout<<"criticalPoints->GetNumberOfPoints() "<<criticalPoints->GetNumberOfPoints()<<endl;
  vtkNew<vtkDoubleArray> criticalPointsTypes;
  criticalPointsTypes->SetNumberOfTuples(criticalPoints->GetNumberOfPoints());
  criticalPointsTypes->SetName("type");
  criticalPoints->GetPointData()->AddArray(criticalPointsTypes);

  vtkNew<vtkProbeFilter> probe;
  probe->SetInputData(criticalPoints);
  probe->SetSourceData(graddataset);
  probe->Update();

  vtkNew<vtkPolyData> seedsFw;
  vtkNew<vtkPoints> seedPointsFw;
  vtkNew<vtkCellArray> seedCellsFw;
  seedsFw->SetPoints(seedPointsFw);
  seedsFw->SetVerts(seedCellsFw);
  vtkNew<vtkPolyData> seedsBw;
  vtkNew<vtkPoints> seedPointsBw;
  vtkNew<vtkCellArray> seedCellsBw;
  seedsBw->SetPoints(seedPointsBw);
  seedsBw->SetVerts(seedCellsBw);

  for (int pointId = 0; pointId < criticalPoints->GetNumberOfPoints(); pointId++)
  {
    //classification
    Eigen::Matrix<double, 3, 3> eigenMatrix;
    int k = 0;
    for (int i = 0; i < 3; i++)
    {
      for (int j = 0; j < 3; j++)
      {
        eigenMatrix(i,j) = probe->GetOutput()->GetPointData()->GetArray("gradient")->GetTuple(pointId)[k];
        k++;
      }
    }

    Eigen::EigenSolver<Eigen::Matrix<double, 3, 3> > eigenS(eigenMatrix);
//    cout << "The first eigenvalue of the 3x3 matrix is:" << endl << eigenS.eigenvalues().row(0) << endl;
//    cout << "The second eigenvalue of the 3x3 matrix is:" << endl << eigenS.eigenvalues().row(1) << endl;
//    cout << "The third eigenvalue of the 3x3 matrix is:" << endl << eigenS.eigenvalues().row(2) << endl;
//    cout << "The first eigenvector of the 3x3 matrix is:" << endl << eigenS.eigenvectors().col(0) << endl;
//    cout << "The second eigenvector of the 3x3 matrix is:" << endl << eigenS.eigenvectors().col(1) << endl;
//    cout << "The third eigenvector of the 3x3 matrix is:" << endl << eigenS.eigenvectors().col(2) << endl;

    int countReal = 0;
    int countComplex = 0;
    int countPos = 0;
    int countNeg = 0;
    for (int i = 0; i < dataset->GetDataDimension(); i++)
    {
      if (imag(eigenS.eigenvalues()[i]) == 0.0)
      {
        countReal++;
      }
      else
      {
        countComplex++;
      }

      if (real(eigenS.eigenvalues()[i]) < -1e-10)
      {
        countNeg++;
      }
      else if (real(eigenS.eigenvalues()[i]) > 1e-10)
      {
        countPos++;
      }
    }
    if (dataset->GetDataDimension() == 2)
    {
      criticalPoints->GetPointData()->GetArray("type")->SetTuple1(pointId, classify2D(countReal, countComplex, countPos, countNeg));
    }
    else
    {
      criticalPoints->GetPointData()->GetArray("type")->SetTuple1(pointId, classify3D(countReal, countComplex, countPos, countNeg));
    }

    // separatrix
    if (criticalPoints->GetPointData()->GetArray("type")->GetTuple1(pointId) == 1 || (dataset->GetDataDimension() == 3 && criticalPoints->GetPointData()->GetArray("type")->GetTuple1(pointId) == 2))
    {
      for (int i = 0; i < dataset->GetDataDimension(); i++)
      {
        double normal[] = {real(eigenS.eigenvectors().col(i)[0]), real(eigenS.eigenvectors().col(i)[1]), real(eigenS.eigenvectors().col(i)[2])};
        if (real(eigenS.eigenvalues()[i]) > 0 && countPos == 1)
        {
          seedPointsFw->InsertNextPoint(dist * real(eigenS.eigenvectors().col(i)[0]) + criticalPoints->GetPoint(pointId)[0],
                                        dist * real(eigenS.eigenvectors().col(i)[1]) + criticalPoints->GetPoint(pointId)[1],
                                        dist * real(eigenS.eigenvectors().col(i)[2]) + criticalPoints->GetPoint(pointId)[2]);
          vtkNew<vtkVertex> vertex0;
          vertex0->GetPointIds()->SetId(0, seedPointsFw->GetNumberOfPoints()-1);
          seedCellsFw->InsertNextCell(vertex0);

          seedPointsFw->InsertNextPoint(-dist * real(eigenS.eigenvectors().col(i)[0]) + criticalPoints->GetPoint(pointId)[0],
                                        -dist * real(eigenS.eigenvectors().col(i)[1]) + criticalPoints->GetPoint(pointId)[1],
                                        -dist * real(eigenS.eigenvectors().col(i)[2]) + criticalPoints->GetPoint(pointId)[2]);
          vtkNew<vtkVertex> vertex1;
          vertex1->GetPointIds()->SetId(0, seedPointsFw->GetNumberOfPoints()-1);
          seedCellsFw->InsertNextCell(vertex1);
          if (dataset->GetDataDimension() == 3)
          {
            surface(1, normal, criticalPoints->GetPoint(pointId), surfaces, dataset, dist, maxNumSteps);
          }
        }
        if (real(eigenS.eigenvalues()[i]) < 0 && countNeg == 1)
        {
          seedPointsBw->InsertNextPoint(dist * real(eigenS.eigenvectors().col(i)[0]) + criticalPoints->GetPoint(pointId)[0],
                                        dist * real(eigenS.eigenvectors().col(i)[1]) + criticalPoints->GetPoint(pointId)[1],
                                        dist * real(eigenS.eigenvectors().col(i)[2]) + criticalPoints->GetPoint(pointId)[2]);
          vtkNew<vtkVertex> vertex0;
          vertex0->GetPointIds()->SetId(0, seedPointsBw->GetNumberOfPoints()-1);
          seedCellsBw->InsertNextCell(vertex0);

          seedPointsBw->InsertNextPoint(-dist * real(eigenS.eigenvectors().col(i)[0]) + criticalPoints->GetPoint(pointId)[0],
                                        -dist * real(eigenS.eigenvectors().col(i)[1]) + criticalPoints->GetPoint(pointId)[1],
                                        -dist * real(eigenS.eigenvectors().col(i)[2]) + criticalPoints->GetPoint(pointId)[2]);
          vtkNew<vtkVertex> vertex1;
          vertex1->GetPointIds()->SetId(0, seedPointsBw->GetNumberOfPoints()-1);
          seedCellsBw->InsertNextCell(vertex1);
          if (dataset->GetDataDimension() == 3)
          {
            surface(0, normal, criticalPoints->GetPoint(pointId), surfaces, dataset, dist, maxNumSteps);
          }
        }
      }
    }
  }
  //  cout<<seeds->GetNumberOfPoints()<<endl;

  vtkNew<vtkStreamTracer> streamTracerFw;
  streamTracerFw->SetInputData(dataset);
  streamTracerFw->SetSourceData(seedsFw);
  streamTracerFw->SetIntegratorTypeToRungeKutta4();
  streamTracerFw->SetIntegrationStepUnit(1);
  streamTracerFw->SetInitialIntegrationStep(dist);
  streamTracerFw->SetIntegrationDirectionToForward();
  streamTracerFw->SetComputeVorticity(0);
  streamTracerFw->SetMaximumNumberOfSteps(maxNumSteps);
  streamTracerFw->SetMaximumPropagation(dist*maxNumSteps);
  streamTracerFw->SetTerminalSpeed(1e-5);
  streamTracerFw->Update();

  vtkNew<vtkDoubleArray> iterationArrayFw;
  iterationArrayFw->SetName("iteration");
  iterationArrayFw->SetNumberOfTuples(streamTracerFw->GetOutput()->GetNumberOfPoints());
  streamTracerFw->GetOutput()->GetPointData()->AddArray(iterationArrayFw);
  vtkNew<vtkDoubleArray> indexArrayFw;
  indexArrayFw->SetName("index");
  indexArrayFw->SetNumberOfTuples(streamTracerFw->GetOutput()->GetNumberOfPoints());
  streamTracerFw->GetOutput()->GetPointData()->AddArray(indexArrayFw);
  for (int i = 0; i < streamTracerFw->GetOutput()->GetNumberOfPoints(); i++)
  {
    iterationArrayFw->SetTuple1(i,i);
    indexArrayFw->SetTuple1(i,0);
  }

  vtkNew<vtkStreamTracer> streamTracerBw;
  streamTracerBw->SetInputData(dataset);
  streamTracerBw->SetSourceData(seedsBw);
  streamTracerBw->SetIntegratorTypeToRungeKutta4();
  streamTracerBw->SetIntegrationStepUnit(1);
  streamTracerBw->SetInitialIntegrationStep(dist);
  streamTracerBw->SetIntegrationDirectionToBackward();
  streamTracerBw->SetComputeVorticity(0);
  streamTracerBw->SetMaximumNumberOfSteps(maxNumSteps);
  streamTracerBw->SetMaximumPropagation(dist*maxNumSteps);
  streamTracerBw->SetTerminalSpeed(1e-5);
  streamTracerBw->Update();

  vtkNew<vtkDoubleArray> iterationArrayBw;
  iterationArrayBw->SetName("iteration");
  iterationArrayBw->SetNumberOfTuples(streamTracerBw->GetOutput()->GetNumberOfPoints());
  streamTracerBw->GetOutput()->GetPointData()->AddArray(iterationArrayBw);
  vtkNew<vtkDoubleArray> indexArrayBw;
  indexArrayBw->SetName("index");
  indexArrayBw->SetNumberOfTuples(streamTracerBw->GetOutput()->GetNumberOfPoints());
  streamTracerBw->GetOutput()->GetPointData()->AddArray(indexArrayBw);
  for (int i = 0; i < streamTracerBw->GetOutput()->GetNumberOfPoints(); i++)
  {
    iterationArrayBw->SetTuple1(i,i);
    indexArrayBw->SetTuple1(i,0);
  }

  // combine lines and surfaces
  vtkNew<vtkAppendPolyData> appendFilter;
  appendFilter->AddInputData(streamTracerFw->GetOutput());
  appendFilter->AddInputData(streamTracerBw->GetOutput());
//  appendFilter->AddInputData(surfaces);
  appendFilter->Update();
  separatrices->DeepCopy(appendFilter->GetOutput());

//  vtkNew<vtkProbeFilter> probe;
  probe->SetInputData(surfaces);
  probe->SetSourceData(dataset);
  probe->Update();
  for (int i = 0; i < dataset->GetPointData()->GetNumberOfArrays(); ++i)
  {
    if (dataset->GetPointData()->GetArray(i)->GetNumberOfComponents() == 3)
    {
//      cout<<dataset->GetPointData()->GetArray(i)->GetNumberOfComponents()<<endl;
      surfaces->GetPointData()->SetVectors(probe->GetOutput()->GetPointData()->GetArray(i));
      break;
    }
  }
//  surfaces->DeepCopy(probe->GetOutput());

  return EXIT_SUCCESS;
}


int main (int argc, char *argv[])
{
  if (argc < 2)
  {
    cout << "use: ./classicFlowTopology <data path> <distance of seeds> <output folder>" << endl;
    return 0;
  }

  // struct stat s;
  // const char* inPath = argv[1];
  // if (stat(inPath, &s) == 0)
  // {
  //   if (s.st_mode & S_IFDIR)
  //   {
  //     // directory
  //   }
  //   else if (s.st_mode & S_IFREG)
  //   {
  //     // file
  //   }
  //   else
  //   {
  //     cout << "enter valid data path: path points to neither directory, nor file" << endl;
  //     return 0;
  //   }
  // }
  // else
  // {
  //   cout << "enter valid data path: invalid path" << endl;
  //   return 0;
  // }

  // char* outPath;
  // if (argc >= 3)
  // {
  //   outPath = argv[2];
  // }
  // else
  // {
  //   strcpy(outPath, inPath);
  //   strcat(outPath, "/classicFlowTopology");
  // }
  
  // if (mkdir(outPath, 0777) == -1)
  // {
  //   cout << strerror(errno) << endl;
  // }
  // else
  // {
  //   cout << "Directory created" << endl;
  // }

  std::string inPath(argv[1]);
  std::string filename(argv[2]); // "data0";
  double dist = atof(argv[3]); // 0.001;
  int maxNumSteps = std::stoi(argv[4]); // 100
  int pad = std::stoi(argv[5]); // 1: true & 0: false
  std::string outPath(argv[6]);

  std::cout << "Reading data from \n" << inPath+filename+".vti" << std::endl;
  std::cout << "The seeds' distance from saddles and integrationstepsize is " << std::to_string(dist) << std::endl;
  std::cout << "The maximum number of iterative integration steps is " << std::to_string(maxNumSteps) << std::endl;
//  std::cout << "pad = " << std::to_string(pad) << std::endl;

  vtkNew<vtkXMLImageDataReader> reader;
  reader->SetFileName((inPath+filename+".vti").c_str());
  reader->Update();
  vtkSmartPointer<vtkImageData> dataset = reader->GetOutput();
  vtkSmartPointer<vtkImageData> origData = reader->GetOutput();
  // dataset->PrintSelf(std::cout,vtkIndent(2));

  if (dataset->GetSpacing()[2] == 0)
  {
    //    cout << "Warning: The spacing in this dataset is 0, which causes Problems in probe."  << endl;
    dataset->SetSpacing(dataset->GetSpacing()[0], dataset->GetSpacing()[1], 1);
  }

  if (pad)
  {
    dataset = padField(dataset, "vectors");
    //    vtkNew<vtkXMLImageDataWriter> imgWriter;
    //    imgWriter->SetInputData(dataset);
    //    imgWriter->SetFileName((outPath+"paddeddataset_"+filename+".vti").c_str());
    //    imgWriter->Write();
  }

  // Triangulate the polydata
  vtkNew<vtkDataSetTriangleFilter> triangulateFilter3D;
  triangulateFilter3D->SetInputData(dataset);
  triangulateFilter3D->Update();
  vtkSmartPointer<vtkUnstructuredGrid> tridataset = triangulateFilter3D->GetOutput();

  // Compute gradient
  vtkNew<vtkGradientFilter> gradient;
  gradient->SetInputData(dataset);
  gradient->SetInputScalars(vtkDataObject::FIELD_ASSOCIATION_POINTS, dataset->GetPointData()->GetVectors()->GetName());
  gradient->SetResultArrayName("gradient");
  gradient->Update();
  vtkSmartPointer<vtkImageData> graddataset = vtkImageData::SafeDownCast(gradient->GetOutput());

  //  vtkNew<vtkXMLUnstructuredGridWriter> ugridWriter;
  //  ugridWriter->SetInputData(tridataset);
  //  ugridWriter->SetFileName((outPath+"tridataset_"+filename+".vtu").c_str());
  // ugridWriter->Write();

  vtkNew<vtkPoints> criticalPointsPoints;
  vtkNew<vtkCellArray> criticalPointsCells;
  vtkNew<vtkPolyData> criticalPoints;
  criticalPoints->SetPoints(criticalPointsPoints);
  criticalPoints->SetVerts(criticalPointsCells);
  vtkNew<vtkPolyData> separatrices;
  vtkNew<vtkPolyData> surfaces;

  if (dataset->GetDataDimension() == 2)
  {
    computeCriticalPoints2D(criticalPoints, tridataset);
  }
  else
  {
    computeCriticalPoints3D(criticalPoints, tridataset);
  }
  if (pad)
  {
    computeSeparatrices(criticalPoints, separatrices, surfaces, dataset, graddataset, dist, maxNumSteps);
  }
  else
  {
    computeSeparatrices(criticalPoints, separatrices, surfaces, origData, graddataset, dist, maxNumSteps);
  }

  vtkNew<vtkXMLPolyDataWriter> polyWriter;
  polyWriter->SetInputData(criticalPoints);
  polyWriter->SetFileName((outPath+"criticalPoints"+filename+".vtp").c_str());
  polyWriter->Write();

  polyWriter->SetInputData(separatrices);
  polyWriter->SetFileName((outPath+"separatrices"+filename+".vtp").c_str());
  polyWriter->Write();

  if (dataset->GetDataDimension() == 3)
  {
    polyWriter->SetInputData(surfaces);
    polyWriter->SetFileName((outPath+"surfaces"+filename+".vtp").c_str());
    polyWriter->Write();
  }

  std::cout << "Exiting... " << std::endl;
  return EXIT_SUCCESS;
}
