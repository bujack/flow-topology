# This script computes the classical flow topology
# The critical points, including the respective types, and separatrices, if exist, are stored in a VTK Multi-Block (.vtm) as separate VTK Poly Block

#import vtk
import sys
# import math
import os
# import subprocess

#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 2:
  print ("use: classicFlowTopology.py <data folder>")
  sys.exit()

scriptPath = os.path.dirname(sys.argv[0])
#print scriptPath
inPath = sys.argv[1]
if not os.path.isdir(inPath):
  print ("enter data folder")
  sys.exit()
#print sorted(os.listdir(inPath))

if len(sys.argv) >= 3:
  dist = float(sys.argv[2])
else:
  dist = 0.01

if len(sys.argv) >= 4:
  maxNumSteps = int(sys.argv[3])
else:
  maxNumSteps = 100

if len(sys.argv) >= 5:
  pad = int(sys.argv[4])
else:
  pad = 0

if len(sys.argv) >= 6:
  outPath = sys.argv[5]
else:
  outPath = inPath + "/classicFlowTopology/"
if not os.path.isdir(outPath):
  os.makedirs(outPath)


#-----------------------------------------------------------------------------------
# main program

for i in sorted(os.listdir(inPath)):
  filename, file_extension = os.path.splitext(i)
  if (file_extension != ".vti"):
    continue
  cmd = "'" + os.path.join(scriptPath, 'build/classicFlowTopology') + "' " + inPath + '/ ' + filename + ' ' + str(dist) + ' ' + str(maxNumSteps) + ' ' + str(pad) + ' ' + outPath
  print ("--", cmd)
  os.system(cmd) # returns the exit status

#p = vtk.vtkPolyData()
#mb = vtk.vtkMultiBlockDataSet()
#mb.SetBlock(0, p);
#writer = vtk.vtkXMLMultiBlockDataWriter()
#writer.SetInputData(mb)
#writer.SetFileName(outPath+"test_"+filename+".vtm");
#writer.Write()



