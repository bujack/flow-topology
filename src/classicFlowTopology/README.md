Use classicFlowTopology/classicFlowTopology.py to get the 2D/3D critical points and its types with separatrices, if exist, following classical flow topology computation.
The type corresponds to the number of positive eigenvalues, i.e. repelling directions

2D Critical Point Types Encoding -
- -1: N/A
- 0: source
- 1: saddle
- 2: sink
- 3: center

3D Critical Point Types Encoding -
- -1: N/A
- 0: source
- 1: saddle with attracting plane
- 2: saddle with repelling plane
- 3: sink
- 4: center

By passing the folder of the datasets, the algorithm will produce a folder called classicFlowTopology and there will be one vtm file per input dataset timestep.

Each vtm file contains two blocks where the first block contains the critical points and their types and the second block contains the separatrices if exist.

** TO RUN **

Compile and replace the classicFlowTopology executable, then run:

python classicFlowTopology.py /path/to/data/folder

