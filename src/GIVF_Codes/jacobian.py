# this script computes the Jacobian, its eigenvalues and eigenvecotors and stores it as a vti file

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import eig

def computeJacobianDeterminant3D(data):
  ew1Array = vtk.vtkDoubleArray()
  ew1Array.SetNumberOfComponents(2)
  ew1Array.SetNumberOfTuples(data.GetNumberOfPoints())
  ew1Array.SetName("eigenvalue1")
  data.GetPointData().AddArray(ew1Array)

  ew2Array = vtk.vtkDoubleArray()
  ew2Array.SetNumberOfComponents(2)
  ew2Array.SetNumberOfTuples(data.GetNumberOfPoints())
  ew2Array.SetName("eigenvalue2")
  data.GetPointData().AddArray(ew2Array)

  ew3Array = vtk.vtkDoubleArray()
  ew3Array.SetNumberOfComponents(2)
  ew3Array.SetNumberOfTuples(data.GetNumberOfPoints())
  ew3Array.SetName("eigenvalue3")
  data.GetPointData().AddArray(ew3Array)

  detArray = vtk.vtkDoubleArray()
  detArray.SetNumberOfComponents(1)
  detArray.SetNumberOfTuples(data.GetNumberOfPoints())
  detArray.SetName("determinant")
  data.GetPointData().AddArray(detArray)

  divArray = vtk.vtkDoubleArray()
  divArray.SetNumberOfComponents(1)
  divArray.SetNumberOfTuples(data.GetNumberOfPoints())
  divArray.SetName("divergence")
  data.GetPointData().AddArray(divArray)

  for i in range(data.GetNumberOfPoints()):
    a = np.array([[data.GetPointData().GetArray("jacobian").GetTuple9(i)[0], data.GetPointData().GetArray("jacobian").GetTuple9(i)[1], data.GetPointData().GetArray("jacobian").GetTuple9(i)[2]], [data.GetPointData().GetArray("jacobian").GetTuple9(i)[3], data.GetPointData().GetArray("jacobian").GetTuple9(i)[4], data.GetPointData().GetArray("jacobian").GetTuple9(i)[5]], [data.GetPointData().GetArray("jacobian").GetTuple9(i)[6], data.GetPointData().GetArray("jacobian").GetTuple9(i)[7], data.GetPointData().GetArray("jacobian").GetTuple9(i)[8]]])
    w, v = eig(a)
    detArray.SetTuple1(i, (w[0]*w[1]*w[2]).real)
    divArray.SetTuple1(i, (w[0]+w[1]+w[2]).real)

    # sort by real part
    idx = w.argsort()[::-1]
#    print (w)
#    print (idx)
    ew1Array.SetTuple2(i, w[idx[0]].real, w[idx[0]].imag)
    ew2Array.SetTuple2(i, w[idx[1]].real, w[idx[1]].imag)
    ew3Array.SetTuple2(i, w[idx[2]].real, w[idx[2]].imag)

  return data


def computeJacobianDeterminant2D(data):
  ew1Array = vtk.vtkDoubleArray()
  ew1Array.SetNumberOfComponents(2)
  ew1Array.SetNumberOfTuples(data.GetNumberOfPoints())
  ew1Array.SetName("eigenvalue1")
  data.GetPointData().AddArray(ew1Array)

  ew2Array = vtk.vtkDoubleArray()
  ew2Array.SetNumberOfComponents(2)
  ew2Array.SetNumberOfTuples(data.GetNumberOfPoints())
  ew2Array.SetName("eigenvalue2")
  data.GetPointData().AddArray(ew2Array)

  detArray = vtk.vtkDoubleArray()
  detArray.SetNumberOfComponents(1)
  detArray.SetNumberOfTuples(data.GetNumberOfPoints())
  detArray.SetName("determinant")
  data.GetPointData().AddArray(detArray)

  divArray = vtk.vtkDoubleArray()
  divArray.SetNumberOfComponents(1)
  divArray.SetNumberOfTuples(data.GetNumberOfPoints())
  divArray.SetName("divergence")
  data.GetPointData().AddArray(divArray)

  for i in range(data.GetNumberOfPoints()):
    a = np.array([[data.GetPointData().GetArray("jacobian").GetTuple9(i)[0], data.GetPointData().GetArray("jacobian").GetTuple9(i)[1]], [data.GetPointData().GetArray("jacobian").GetTuple9(i)[3], data.GetPointData().GetArray("jacobian").GetTuple9(i)[4]]])
    w, v = eig(a)
    idx = w.argsort()[::-1]
    ew1Array.SetTuple2(i, w[idx[0]].real, w[idx[0]].imag)
    ew2Array.SetTuple2(i, w[idx[1]].real, w[idx[1]].imag)

    detArray.SetTuple1(i, (w[0]*w[1]).real)
    divArray.SetTuple1(i, (w[0]+w[1]).real)

  return data


#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 2:
  print ("use: jacobian.py <filepath to folder that contains the data>")
  sys.exit()

input = sys.argv[1]
if not os.path.isdir(input):
  print ("enter filepath to folder that contains the data as first argument")
  sys.exit()
#print sorted(os.listdir(input))

if len(sys.argv) == 3:
  output = sys.argv[2]
else:
  output = input + '/jacobian'
if not os.path.isdir(output):
  os.makedirs(output)

#-----------------------------------------------------------------------------------
# main program

for i in sorted(os.listdir(input)):
  filename, file_extension = os.path.splitext(i)
  if (file_extension != ".vti"):
    continue
  reader = vtk.vtkXMLImageDataReader()
  reader.SetFileName(input + '/' + i)
  reader.Update()
  data = reader.GetOutput()
#  print "\n", i

  # compute gradient
  gradientFilter = vtk.vtkGradientFilter()
  gradientFilter.SetInputData(data)
  gradientFilter.SetInputScalars(vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, data.GetPointData().GetVectors().GetName())
  gradientFilter.SetResultArrayName("jacobian");
  gradientFilter.Update()

  if data.GetDataDimension() == 2:
    data = computeJacobianDeterminant2D(gradientFilter.GetOutput())
  else:
    data = computeJacobianDeterminant3D(gradientFilter.GetOutput())

#  print (data)
#  print (output + '/' + i)


  writer = vtk.vtkXMLImageDataWriter()
  writer.SetInputData(data)
  writer.SetFileName(output + '/' + i)
  writer.Write()
