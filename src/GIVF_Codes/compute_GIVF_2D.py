# This ParaView trace is generated using ParaView version 5.6.1 with TTK 0.9.8

from paraview.simple import *
import vtk
import numpy as np
import sys
import math
import os
import glob
################################################################################################

th1 = -0.1 ## threshold on PairIdentifier values. this just takes out the diagonal
th2 = 0.0 ## threshold on Persistence values. if set to zero, no pruning is performed
exponent = 2 ## exponent for distance function

################################################################################################

# this function interpolates vectors using vtk probe filter
def interpolate_vec(data,probedata):
    #vec_array_name = 'vectors'
    probe = vtk.vtkProbeFilter()
    probe.SetInputData(probedata)
    probe.SetSourceData(data)
    probe.Update()
    return probe.GetOutput().GetPointData().GetVectors().GetTuple3(0) 

# this function returns the vetor given a physical position
def get_vector(seed_physical,data):
    pointArray = vtk.vtkPoints()
    pointArray.InsertNextPoint(seed_physical)
    probepolydata = vtk.vtkPolyData()
    probepolydata.SetPoints(pointArray)
    interp_vec = interpolate_vec(data,probepolydata)
    return np.asarray(interp_vec)

# compute euclidean distance between two points
def get_distance(loc1,loc2,exponent):
    length = len(loc1)
    val = 0
    for i in range(length):
        val = val + math.pow((loc1[i] - loc2[i]),exponent)
    return val
        
# Writes a vtkimagedata to disk
def write_vti(imagedata,filename):
  writer = vtk.vtkXMLImageDataWriter()
  writer.SetFileName(filename)
  writer.SetInputData(imagedata)
  writer.Write()    
    
def read_vti(filename):
    reader = vtk.vtkXMLImageDataReader()
    reader.SetFileName(filename)
    reader.Update()
    return reader.GetOutput()

def read_vtu(filename):
    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(filename)
    reader.Update()
    return reader.GetOutput()

def filter_critical_points(critdata, vecdata):
    locs = []
    vecs = []
    types = []
    num=0
    
    ## Filter vectors and locations of critical points to be used
    ## conditionas for filtering points
    ## (1) crit_index=1 --> remove (this is scalar saddle and we do not want them)
    ## (2) crit_index=0 and determinant=negative --> keep
    ## (3) crit_index=2 and determinant=positive --> keep
    for i in range(critdata.GetNumberOfPoints()):
        crit_index = critdata.GetPointData().GetArray('CriticalType').GetTuple1(i)
        ## check to exclude the saddle: crit_index==1 means scalar saddle
        if crit_index != 1:
            determinant_val = critdata.GetPointData().GetArray('determinant').GetTuple1(i)
            if crit_index == 0 and determinant_val < 0:
                pts = critdata.GetPoint(i)
                vect = get_vector(pts,vecdata)
                locs.append(pts)
                vecs.append(vect)
                types.append(crit_index)
            if crit_index >= 2 and determinant_val > 0:
                pts = critdata.GetPoint(i)
                vect = get_vector(pts,vecdata)
                locs.append(pts)
                vecs.append(vect)
                types.append(crit_index)
    return locs,vecs,types

## computes the GIVF field
def generate_GIVF(vecdata,vecs,locs,exponent=2):
    
    totNumPts = vecdata.GetNumberOfPoints()

    vectors = vtk.vtkDoubleArray()
    vectors.SetNumberOfComponents(3)
    vectors.SetNumberOfTuples(vecdata.GetNumberOfPoints())
    vectors.SetName("vectors_GIVF")
    
    ## now do the subtraction
    for i in range(totNumPts):
        current_vec = vecdata.GetPointData().GetVectors().GetTuple3(i)
        current_pts = vecdata.GetPoint(i)
        avg_vec = np.zeros_like(locs[0])
        sum_weight=0
        
        flag=0
        for j in range(len(locs)):
            dist = get_distance(locs[j],current_pts,exponent)
            
            if dist > 0:
                weight = 1.0/dist
                avg_vec = avg_vec + weight*vecs[j]
                sum_weight = sum_weight + weight
            
            elif dist == 0:
                flag=1
                break
                
        if flag == 0:
            avg_vec = avg_vec/sum_weight    
            new_vec = current_vec -  avg_vec
        else:
            new_vec = current_vec
        
        vectors.SetTuple3(i, new_vec[0], new_vec[1], new_vec[2])

    ## create a new vector vti data
    outdata = vtk.vtkImageData()
    outdata.SetOrigin(vecdata.GetOrigin())
    outdata.SetExtent(vecdata.GetExtent())
    outdata.SetSpacing(vecdata.GetSpacing())
    outdata.GetPointData().SetVectors(vectors)  
    return outdata

def write_filtered_crit_pts(locs,ctypes,fname):
  pdata = vtk.vtkPolyData()
  pts = vtk.vtkPoints()

  types = vtk.vtkIntArray()
  types.SetName('CriticalType')

  for i in range(len(locs)):
    pts.InsertNextPoint(locs[i])
    types.InsertNextTuple1(ctypes[i])

  pdata.SetPoints(pts)  
  pdata.GetPointData().AddArray(types)

  writer = vtk.vtkXMLPolyDataWriter()
  writer.SetInputData(pdata)
  writer.SetFileName(fname)
  writer.Write()


##########################################################################################
if len(sys.argv) < 2:
  print ('use: compute_GIVF_2D.py <filepath to folder that contains the data>')
  sys.exit()

data_path = sys.argv[1]
if not os.path.isdir(data_path):
  print ('enter filepath to folder that contains the data as first argument')
  sys.exit()

if len(sys.argv) == 2:
  data_out_path = data_path + '/givf_fields'
elif len(sys.argv) == 3:
  data_out_path = sys.argv[2]
elif len(sys.argv) == 4:
  persistence_val = float(sys.argv[3])
  th2 = persistence_val
  data_out_path = sys.argv[2]
 
print ('persistence threshold value: ' + str(th2))


if not os.path.isdir(data_out_path):
  os.makedirs(data_out_path)

scriptPath = os.path.dirname(sys.argv[0])
print (scriptPath)
####################################################################################
#compute jacobian first here
jacobian_out_temp_path = data_out_path + '/jacobian_fields'
if not os.path.isdir(jacobian_out_temp_path):
  os.makedirs(jacobian_out_temp_path)  

command = 'vtkpython ' + os.path.join(scriptPath, 'jacobian.py') + ' ' + data_path + ' ' + jacobian_out_temp_path
#print ("--", command)
os.system(command)
#####################################################################################

data_path = jacobian_out_temp_path

crit_pts_path = data_out_path + '/crit_pts'
if not os.path.isdir(crit_pts_path):
  os.makedirs(crit_pts_path)  

## main loop that computes the GIVF
for i in sorted(os.listdir(data_path)):
  filename, file_extension = os.path.splitext(i)
  if (file_extension != '.vti'):
    continue

  inpfile = data_path + '/' + i

  ####################################################################
  outfile = crit_pts_path + '/critpts_' + filename + '.vtu'
  outfile1 = jacobian_out_temp_path + '/pdiagram_op.vtu'
  out_givf_file = data_out_path + '/givf_' + i
  #####################################################################

  # create a new 'XML Image Data Reader'
  data0vti = XMLImageDataReader(FileName=[inpfile])
  data0vti.PointArrayStatus = ['vectors', 'jacobian', 'eigenvalues', 'determinant', 'divergence']

  # create a new 'Tetrahedralize'
  tetrahedralize1 = Tetrahedralize(Input=data0vti)

  # create a new 'TTK PersistenceDiagram'
  tTKPersistenceDiagram1 = TTKPersistenceDiagram(Input=tetrahedralize1)
  tTKPersistenceDiagram1.ScalarField = 'determinant'
  tTKPersistenceDiagram1.InputOffsetField = 'determinant'

  # save persistence diagram
  SaveData(outfile1, proxy=tTKPersistenceDiagram1)

  ## load persistence diagram
  reader = vtk.vtkXMLUnstructuredGridReader()
  reader.SetFileName(outfile1)
  reader.Update()
  persistence_diagram = reader.GetOutput()
  pairIdentifier_range = persistence_diagram.GetCellData().GetArray('PairIdentifier').GetRange()
  print ('Pair identifier range is: ' + str(pairIdentifier_range))
  persistence_range = persistence_diagram.GetCellData().GetArray('Persistence').GetRange()
  print ('Persistence range is: ' + str(persistence_range))

  # create a new 'Threshold'
  threshold1 = Threshold(Input=tTKPersistenceDiagram1)
  threshold1.Scalars = ['CELLS', 'PairIdentifier']
  threshold1.ThresholdRange = [th1, pairIdentifier_range[1]]

  # create a new 'Threshold'
  threshold2 = Threshold(Input=threshold1)
  threshold2.Scalars = ['CELLS', 'Persistence']
  threshold2.ThresholdRange = [th2, persistence_range[1]]

  # create a new 'TTK TopologicalSimplification'
  tTKTopologicalSimplification1 = TTKTopologicalSimplification(Domain=tetrahedralize1, Constraints=threshold2)
  tTKTopologicalSimplification1.ScalarField = 'determinant'
  tTKTopologicalSimplification1.InputOffsetField = 'determinant'
  tTKTopologicalSimplification1.Vertexidentifierfield = 'CriticalType'
  tTKTopologicalSimplification1.OutputOffsetScalarField = ''

  # create a new 'TTK ScalarFieldCriticalPoints'
  tTKScalarFieldCriticalPoints1 = TTKScalarFieldCriticalPoints(Input=tTKTopologicalSimplification1)
  tTKScalarFieldCriticalPoints1.ScalarField = 'determinant'
  tTKScalarFieldCriticalPoints1.InputOffsetfield = 'determinant'

  # save filtered critical points out
  SaveData(outfile, proxy=tTKScalarFieldCriticalPoints1)

  ###################################################################################
  ## Now compute the GIVF field

  ## load vector data
  vecdata = read_vti(inpfile)
  ## load criical point data
  critdata = read_vtu(outfile)
  print ('Data loading done')

  ## Filter critical points
  locs, vecs, types = filter_critical_points(critdata, vecdata)
  print ('Total number of points left after filtering: ' + str(len(vecs)))

  ## generate the GIVF
  outdata = generate_GIVF(vecdata,vecs,locs,exponent)
  outdata.GetPointData().AddArray(vecdata.GetPointData().GetArray('determinant'))

  ## write data out
  write_vti(outdata,out_givf_file)
  print ('Generated GIVF field')

  fname = data_out_path + '/filtered_critical_points'
  if not os.path.isdir(fname):
    os.makedirs(fname)  
  
  fname = fname + '/'  + filename + '_crit_pts.vtp'
  write_filtered_crit_pts(locs,types,fname)


## finally remove the jacobian temp folder
command = 'rm -rf ' + jacobian_out_temp_path
os.system(command)

## finally remove the critical point temp folder
command = 'rm -rf ' + crit_pts_path
os.system(command)
