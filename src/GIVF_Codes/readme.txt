This readme describes how to run the GIVF codes. 

The scripts compute_GIVF_2D.py and compute_GIVF_2D_New.py first call the jacobian.py script internally to compute the jacobian fields. Then they take those fields and comput the GIVFs.

Run the script: pvpython compute_GIVF_2D.py ../../testSuite/2DExtended/SaddleAcceleratedTranslation/ GIVF_fields 0.005
                pvpython compute_GIVF_2D_New.py ../../testSuite/2DExtended/SaddleAcceleratedTranslation/ GIVF_fields 0.005

The compute_GIVF_3D.py first calls the jacobian.py script internally to compute the jacobian fields. Then it takes those fields and computs the GIVFs.

Run the script: pvpython compute_GIVF_3D.py ../../testSuite/2DExtended/SaddleAcceleratedTranslation/ GIVF_fields 0.005

first parameter = location to the folder where the vector fields are
second parameter = location to the folder where the final GIVF fields will be produced (if not provided, output will be in the input data folder)
third parameter = persistence threshold value (if not provided, assumed 0.0)

Run jacobian.py to compute the determinant of the Jacobian through
vtk/build/bin/vtkpython flowTopology/src/jacobian.py data/test analysis/test