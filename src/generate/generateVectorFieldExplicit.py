# this script produces a 2D time-depenendent vector field on [-1,1]^2 from an anlytic formula as a vti file

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import inv
from helper import *


#-----------------------------------------------------------------------------------
# paramters
numberOfTimeSteps = 0
resolution = 50
print ("the parameters of this run of generateVectorField2D are:")
print ("numberOfTimeSteps =", numberOfTimeSteps)
print ("resolution =", resolution)


#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) != 2:
  print ("use: generateVectorFieldExplicit.py <filepath to folder that will contain the data>")
  sys.exit()

input = sys.argv[1]
if not os.path.isdir(input):
  os.makedirs(input)


# sum from pacificVis 2016
def generateVectorField(x, t, numberOfTimeSteps):

  # vatistas 2 sink -1,1
  vSink = -1. / ( math.sqrt(1 + math.pow((x[0]+1)*(x[0]+1) + (x[1]-1)*(x[1]-1), 2)) ) * np.array([x[0]+1, x[1]-1, 0])

  # kaufman/scully source 1,-1
  vSource = math.sqrt(0.5) / ( 1 + (x[0]-1)*(x[0]-1) + (x[1]+1)*(x[1]+1) ) * np.array([(x[0]-1) + (x[1]+1), (x[1]+1) - (x[0]-1), 0])

  # lamb oseen vortex -1,-1
  if ((x[0]+1)*(x[0]+1) + (x[1]+1)*(x[1]+1)) == 0:
    vVortex = np.array([0,0,0])
  else:
    vVortex = (1 - math.exp( -(x[0]+1)*(x[0]+1) - (x[1]+1)*(x[1]+1) )) / ((x[0]+1)*(x[0]+1) + (x[1]+1)*(x[1]+1)) * np.array([-(x[1]+1), (x[0]+1), 0])

  # gaussian saddle from the formula 1,1
  vSaddle = math.exp( -(x[0]-1)*(x[0]-1) - (x[1]-1)*(x[1]-1) ) * np.array([-(x[0]-1), (x[1]-1), 0])
  # gaussian saddle that looks correct 1,1
  vSaddle = math.exp( -(x[0]-1)*(x[0]-1) - (x[1]-1)*(x[1]-1) ) * np.array([-(x[1]-1), -(x[0]-1), 0])

  # sheer flow
  if x[0] >= 0.5:
    sheer = 0.5
  if x[0] <= -0.5:
    sheer = -0.5
  if x[0] > -0.5 and x[0] < 0.5:
    sheer = 0.5 * np.sin(math.pi * x[0])
  vSheer = np.array([0, sheer, 0])

#  v1 = math.sqrt(1./2)/(1+math.pow((x[0]-1),2)+math.pow((x[1]+1),2))*((x[0]-1)+(x[1]+1)) -1./(math.sqrt(1+math.pow(math.pow((x[0]+1),2)+math.pow((x[1]-1),2),2)))*(x[0]+1) -math.exp(-math.pow((x[0]-1),2)-math.pow((x[1]-1),2))*(x[0]-1) -(1-math.exp(-math.pow((x[0]+1),2)-math.pow((x[1]+1),2)))/(1e-10+math.pow((x[0]+1),2)+math.pow((x[1]+1),2))*(x[1]+1)
#
#  v2 = math.sqrt(1./2)/(1+math.pow((x[0]-1),2)+math.pow((x[1]+1),2))*((x[1]+1)-(x[0]-1)) -1./(math.sqrt(1+math.pow(math.pow((x[0]+1),2)+math.pow((x[1]-1),2),2)))*(x[1]-1) +math.exp(-math.pow((x[0]-1),2)-math.pow((x[1]-1),2))*(x[1]-1) +(1-math.exp(-math.pow((x[0]+1),2)-math.pow((x[1]+1),2)))/(1e-10+math.pow((x[0]+1),2)+math.pow((x[1]+1),2))*(x[0]+1)
#  return [v1,v2+sheer,0]


  return (vSink + vSource + vVortex + vSaddle + vSheer)
  return (vSaddle)


## quad gyre
#def generateVectorField(x, t, numberOfTimeSteps):
##  t=0.5*t
#  x[0] = x[0] + 1
##  if x[0] == 1 and x[1] == 0:
##    print t
##    return [-1e-2,0,0]
#  eps = 0.25
#  A = 1. / numberOfTimeSteps
#  omega = 2. * math.pi / numberOfTimeSteps
#  f = eps * np.sin(omega*t) * x[0] * x[0] + (1 - 2 * eps * np.sin(omega*t)) * x[0]
#  fD = 2 * eps * np.sin(omega*t) * x[0] + 1 - 2 * eps * np.sin(omega*t)
#  return (-A * math.pi * np.sin(math.pi*f) * np.cos(math.pi*x[1]), A * math.pi * np.cos(math.pi*f) * np.sin(math.pi*x[1]) * fD, 0)



## tobias saddle
#def generateVectorField(x, t, numberOfTimeSteps):
#  theta = 2 * math.pi / numberOfTimeSteps / numberOfTimeSteps
#  c, s = np.cos(theta * t * t), np.sin(theta * t * t)
#  b = 0.5 * np.array([s + 1, c, 0])
#  bD = 0.5 * np.array([c, -s, 0]) * 2 * theta * t
#  xNeu = [x[0]+b[0], x[1]+b[1], 0]
#  v = np.array([xNeu[0], -xNeu[1] , 0])
#
#  return v - bD

## accelerated saddle
#def generateVectorField(x, t, numberOfTimeSteps):
#  theta = 0.02 * math.pi
#  c, s = np.cos(theta * t * t), np.sin(theta * t * t)
#  b = 0.5 * np.array([s + 1, c, 0])
#  bD = 0.5 * np.array([c, -s, 0]) * 2 * theta * t
#  xNeu = [x[0]+0.5+b[0], x[1]+b[1], 0]
#  e = math.exp(-2 * np.linalg.norm(xNeu)) * 20 / numberOfTimeSteps
#  v = np.array([xNeu[0], -xNeu[1] , 0])
#
##  v[0] = x[0] - 0.5 * np.sin(theta*t*t)
##  v[1] = -x[1] + 0.5 * np.cos(theta*t*t)
##  bD[0] = theta * t * np.cos(theta*t*t)
##  bD[1] = - theta * t * np.sin(theta*t*t)
##  e = math.exp(-2 * np.linalg.norm([x[0] - 0.5 * np.sin(theta*t*t), -x[1] + 0.5 * np.cos(theta*t*t), 0])) * 2
#
#  return v * e - bD

## shear
#def generateVectorField(x, t, numberOfTimeSteps):
#  e = math.exp(-2 * np.linalg.norm(x)) * 20 / numberOfTimeSteps
##  v = 0.1 * np.array([-1, -x[1]*math.sin(x[0]), 0])
##  return np.array([x[0]+0.1*x[1], 0, 0])
##  if abs(x[0]) * abs(x[1]) < 1e-10: return np.array([0, 0, 0])
##  else:
#  return np.array([abs(x[0]), 0, 0])*e
#  if t<5: return np.array([0,-abs(x[1]), 0])*e
#  else: return np.array([-abs(x[0]),0, 0])*e
#  if t<5: return np.array([abs(x[0]), 0, 0])*e
#  else: return np.array([0, abs(x[1]), 0])*e
#  if abs(x[0])>abs(x[1]):
#    if x[0] > 0: return np.array([0, -1, 0])*e*abs(x[0]-x[1])*abs(x[0]+x[1])
#    else: return np.array([0, 1, 0])*e*abs(x[0]-x[1])*abs(x[0]+x[1])
#  else:
#    if x[1] > 0: return np.array([-1, 0, 0])*e*abs(x[0]-x[1])*abs(x[0]+x[1])
#    else: return np.array([1, 0, 0])*e*abs(x[0]-x[1])*abs(x[0]+x[1])
#  return np.array([y[0]*abs(x[1]), -x[1]*abs(x[0]), 0])*e
#  v = np.array([x[0], 0, 0])
#  if x[0]>=0: return v*e
#  else: return -v*e
##  v = 0.1*np.array([x[0]*x[0], x[0]*x[1], 0])
#  v = np.array([0, x[1], 0])
#  if x[1]>0: return v*e
#  else: return -v*e
#
#  return v

## saddle
#def generateVectorField(x, t, numberOfTimeSteps):
#  if np.linalg.norm(x) < 1e-5:
#    return np.array([0, 0 , 0])
#  e = math.exp(-2 * np.linalg.norm(x)) * 20 / numberOfTimeSteps
#  v = np.array([-x[0], x[1] , 0])
#  return v * e

## pseudo steady saddle
#def generateVectorField(x, t, numberOfTimeSteps):
#  x1 = np.array([x[0] + 0.5, x[1], x[2]])
#  theta = 2 * math.pi / numberOfTimeSteps
#  c, s = np.cos(theta * t), np.sin(theta * t)
#  b = 0.5 * np.array([s + 1, c, 0])
#
#  x1 = np.array([x[0], x[1]-0.5, x[2]])
#  e = math.exp(-2 * np.linalg.norm(x1)) * 20 / numberOfTimeSteps
##  v = np.array([x1[0] - b[0], -(x1[1] - b[1]), 0])
#  v = np.array([-x1[0], x1[1], 0])
#
##  x1 = np.array([x[0] - (0.9-0.1*t), x[1], x[2]])
##  e = math.exp(-2 * np.linalg.norm(x1)) * 20 / numberOfTimeSteps
##  #  v = np.array([x1[0] - b[0], -(x1[1] - b[1]), 0])
##  v = np.array([x1[0], -x1[1], 0])
#
#  return v * e

## dipole
#def generateVectorField(x, t, numberOfTimeSteps):
#  e = math.exp(-2 * np.linalg.norm(x)) * 20 / numberOfTimeSteps
#  v = np.array([x[0]*x[0] - x[1]*x[1] , 2*x[0]*x[1], 0])
#  return v * e

## blue sky bifurcation
#def generateVectorField(x, t, numberOfTimeSteps):
#  e = math.exp(-2 * np.linalg.norm(x)) * 20 / numberOfTimeSteps
#  v = np.array([(t-0.5*numberOfTimeSteps)/numberOfTimeSteps * x[0] + 0.5 * x[1] , (t-0.5*numberOfTimeSteps)/numberOfTimeSteps * x[1] - 0.5 * x[0], 0])
#  return v * e

#-----------------------------------------------------------------------------------
# main program

for j in range(numberOfTimeSteps+1):
  dataSet = vtk.vtkImageData()
  dataSet.SetOrigin(0,0,0)
  dataSet.SetExtent(-resolution/2,resolution/2,-resolution/2,resolution/2,0,0)
  #  dataSet.SetExtent(-2*resolution, 2*resolution, -2*resolution, 2*resolution, 0, 0)
  dataSet.SetExtent(-resolution, resolution, -resolution, resolution, 0, 0)
  dataSet.SetSpacing(2./resolution,2./resolution,1)
#  print dataSet.GetBounds()

  vectors = vtk.vtkDoubleArray()
  vectors.SetNumberOfComponents(3)
  vectors.SetNumberOfTuples(dataSet.GetNumberOfPoints())
  vectors.SetName("vectors")
  dataSet.GetPointData().SetVectors(vectors)

  for i in range(dataSet.GetNumberOfPoints()):
    v = generateVectorField(np.array(dataSet.GetPoint(i)), j, numberOfTimeSteps)
    vectors.SetTuple3( i, v[0], v[1], v[2]  );

  writer = vtk.vtkXMLImageDataWriter()
  writer.SetFileName(input + "/data" + str(j) + ".vti")
  writer.SetInputData(dataSet)
  writer.Write()
