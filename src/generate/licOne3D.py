# trace generated using paraview version 5.6.0
#
# To ensure correct image size when batch processing, please search
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
import os
import re
from paraview.simple import *
LoadPlugin('libSurfaceLIC.dylib', remote=True, ns=globals())

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)


#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

folder = sys.argv[1]
field = sys.argv[2]
transformation = sys.argv[3]
pathName = folder + '/data0.vti'
fileList = sorted_aphanumeric(os.listdir(folder))
pathList = []
for i in fileList:
  filename, file_extension = os.path.splitext(i)
  if (file_extension == ".vti"):
    pathList.append(folder + '/' + i)
#print pathList

# create a new 'XML Image Data Reader'
data0vti = XMLImageDataReader(FileName=pathList)
data0vti.PointArrayStatus = ['vectors']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1000, 1000]

# show data in view
slice1Display = Show(data0vti, renderView1)

# get color transfer function/color map for 'vectors'
vectorsLUT = GetColorTransferFunction('vectors')

# get opacity transfer function/opacity map for 'vectors'
vectorsPWF = GetOpacityTransferFunction('vectors')

# create a new 'Slice'
slice1 = Slice(Input=data0vti)
slice1.SliceType = 'Plane'
slice1.SliceOffsetValues = [0.0]

# show data in view
slice1Display = Show(slice1, renderView1)

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'
slice1Display.ColorArrayName = [None, '']
slice1Display.OSPRayScaleArray = 'vectors'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.SelectOrientationVectors = 'vectors'
slice1Display.ScaleFactor = 0.2
slice1Display.SelectScaleArray = 'None'
slice1Display.GlyphType = 'Arrow'
slice1Display.GlyphTableIndexArray = 'None'
slice1Display.GaussianRadius = 0.01
slice1Display.SetScaleArray = ['POINTS', 'vectors']
slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
slice1Display.OpacityArray = ['POINTS', 'vectors']
slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.SelectionCellLabelFontFile = ''
slice1Display.SelectionPointLabelFontFile = ''
slice1Display.PolarAxes = 'PolarAxesRepresentation'
slice1Display.SelectInputVectors = ['POINTS', 'vectors']
slice1Display.WriteLog = ''

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
slice1Display.DataAxesGrid.XTitleFontFile = ''
slice1Display.DataAxesGrid.YTitleFontFile = ''
slice1Display.DataAxesGrid.ZTitleFontFile = ''
slice1Display.DataAxesGrid.XLabelFontFile = ''
slice1Display.DataAxesGrid.YLabelFontFile = ''
slice1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
slice1Display.PolarAxes.PolarAxisTitleFontFile = ''
slice1Display.PolarAxes.PolarAxisLabelFontFile = ''
slice1Display.PolarAxes.LastRadialAxisTextFontFile = ''
slice1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [1.0, 0.0, 1.0]

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [1.0, 0.0, 1.0]

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [0.0, 0.0, 1.0]

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [0.0, 0.0, 1.0]

# update the view to ensure updated data information
renderView1.Update()

# change representation type
slice1Display.SetRepresentationType('Surface LIC')

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
vectorsLUT.ApplyPreset('Spectral_lowBlue', True)

# rescale color and/or opacity maps used to exactly fit the current data range
slice1Display.RescaleTransferFunctionToDataRange(False, True)

# Properties modified on slice1Display
slice1Display.NumberOfSteps = 200

# Properties modified on slice1Display
slice1Display.ColorMode = 'Multiply'

# Properties modified on slice1Display
slice1Display.EnhanceContrast = 'LIC and Color'

# Properties modified on slice1Display
slice1Display.GenerateNoiseTexture = 1

# Properties modified on slice1Display
slice1Display.NoiseType = 'Perlin'

# Properties modified on slice1Display
slice1Display.NoiseTextureSize = 400

# Properties modified on slice1Display
slice1Display.NoiseGrainSize = 32

# set active source
SetActiveSource(data0vti)

# create a new 'Glyph'
glyph1 = Glyph(Input=data0vti,
               GlyphType='Arrow')
glyph1.OrientationArray = ['POINTS', 'vectors']
glyph1.ScaleArray = ['POINTS', 'No scale array']
glyph1.ScaleFactor = 0.2
glyph1.GlyphTransform = 'Transform2'

# show data in view
glyph1Display = Show(glyph1, renderView1)

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = [None, '']
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'None'
glyph1Display.ScaleFactor = 0.22010403871536255
glyph1Display.SelectScaleArray = 'None'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'None'
glyph1Display.GaussianRadius = 0.011005201935768127
glyph1Display.SetScaleArray = [None, '']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = [None, '']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.SelectionCellLabelFontFile = ''
glyph1Display.SelectionPointLabelFontFile = ''
glyph1Display.PolarAxes = 'PolarAxesRepresentation'
glyph1Display.SelectInputVectors = [None, '']
glyph1Display.WriteLog = ''

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
glyph1Display.DataAxesGrid.XTitleFontFile = ''
glyph1Display.DataAxesGrid.YTitleFontFile = ''
glyph1Display.DataAxesGrid.ZTitleFontFile = ''
glyph1Display.DataAxesGrid.XLabelFontFile = ''
glyph1Display.DataAxesGrid.YLabelFontFile = ''
glyph1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
glyph1Display.PolarAxes.PolarAxisTitleFontFile = ''
glyph1Display.PolarAxes.PolarAxisLabelFontFile = ''
glyph1Display.PolarAxes.LastRadialAxisTextFontFile = ''
glyph1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# Properties modified on glyph1
glyph1.ScaleArray = ['POINTS', 'vectors']

# Properties modified on glyph1
glyph1.ScaleFactor = 0.5

# Properties modified on glyph1
glyph1.MaximumNumberOfSamplePoints = 200

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.TipRadius = 0.3

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.TipLength = 0.3

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.ShaftRadius = 0.1

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.TipResolution = 20

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.ShaftResolution = 20

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(data0vti)

# set active source
SetActiveSource(slice1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(slice1Display, ('POINTS', 'vectors', 'Magnitude'))

# rescale color and/or opacity maps used to include current data range
slice1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'vectors'
vectorsLUT = GetColorTransferFunction('vectors')

# get opacity transfer function/opacity map for 'vectors'
vectorsPWF = GetOpacityTransferFunction('vectors')

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [-0.02109164118593961, -0.061341969165280265, 0.9978939349905928]

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [-0.02109164118593961, -0.061341969165280265, 0.9978939349905928]

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [-0.021277138302823833, -0.0674377449199983, 0.9974965834256014]

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [-0.021277138302823833, -0.0674377449199983, 0.9974965834256014]

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(data0vti)

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [-1.9849980323497092, -1.7125751868768386, 6.157229793639999]
renderView1.CameraViewUp = [-0.03093942294772003, 0.9654970652790097, 0.25856946657577756]
renderView1.CameraParallelScale = 1.7320508075688772



# update the view to ensure updated data information
renderView1.Update()

# current camera placement for renderView1
renderView1.InteractionMode = '3D'
renderView1.CameraPosition = [-1.5, -1.5, 5.5]
renderView1.CameraParallelScale = 1

# save screenshot
SaveScreenshot(folder + '/' + str(field) + str(transformation) + 'Lic.png', renderView1, ImageResolution=[1000, 1000], OverrideColorPalette='BlackBackground')

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# save animation
SaveAnimation(folder + '/' + 'Lic.png', renderView1, ImageResolution=[1000, 1000], OverrideColorPalette='BlackBackground', FrameRate=5)

submitCommand = 'ffmpeg -y -r 5 -i ' + folder + '/Lic.0%03d.png -vcodec libx264 -r 30 -pix_fmt yuv420p ' + folder + '/' + str(field) + str(transformation) + 'LicMovie.mp4'
print '--submitting: ' + submitCommand
os.system(submitCommand)

for f in os.listdir(folder):
  if f.find('Lic') == 0:
    os.remove(folder + '/' + f)

#
##### saving camera placements for all active views
#
## current camera placement for renderView1
#renderView1.InteractionMode = '2D'
#renderView1.CameraPosition = [0.0, 0.0, 10000.0]
#renderView1.CameraParallelScale = 1.4142135623730947
#
##### uncomment the following to render all views
## RenderAllViews()
## alternatively, if you want to write images, you can use SaveScreenshot(...).
