# this script produces a 2D time-depenendent vector field on [-1,1]^2 from an anlytic formula as a vti file

import vtk
import numpy as np
import sys
import math
import os

def center(x, t, numberOfTimeSteps):
  v = np.array([-x[1], x[0], 0])
  J = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 0]])
  vt = np.array([0, 0, 0])
  return (v, J, vt)

def saddle(x, t, numberOfTimeSteps):
  v = np.array([x[0], -x[1], x[2]])
  J = np.array([[1, 0, 0], [0, -1, 0], [0, 0, 0]])
  vt = np.array([0, 0, 0])
  return (v, J, vt)

def sink(x, t, numberOfTimeSteps):
  v = np.array([-x[0], -x[1], -x[2]])
  J = np.array([[-1, 0, 0], [0, -1, 0], [0, 0, 0]])
  vt = np.array([0, 0, 0])
  return (v, J, vt)

def source(x, t, numberOfTimeSteps):
  v = np.array([x[0], x[1], x[2]])
  J = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 0]])
  vt = np.array([0, 0, 0])
  return (v, J, vt)

def test(x, t, numberOfTimeSteps):
#  v = np.array([0.5, 0, 0])
#  J = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
#  vt = np.array([0, 0, 0])
#  return (v, J, vt)
  v = np.array([-x[0], -x[1], 0])
  J = np.array([[-1, 0, 0], [0, -1, 0], [0, 0, 0]])
  vt = np.array([0, 0, 0])
  return (v, J, vt)


fieldOptions = {"Center": center, "Saddle": saddle, "Sink": sink, "Source": source, "Test": test}

# no transformation
def nothing(t, numberOfTimeSteps):
  b = np.array([0, 0, 0])
  bD = np.array([0, 0, 0])
  bDD = np.array([0, 0, 0])
  A = np.array([[1, 0 , 0], [0, 1 , 0], [0, 0, 1]])
  AD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
  ADD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
  return (b, bD, bDD, A, AD, ADD)

# no movement but translation to (0,0.5) and rotation by pi/4
def steady(t, numberOfTimeSteps):
  b = np.array([0, 0.5, 0])
  bD = np.array([0, 0, 0])
  bDD = np.array([0, 0, 0])
  theta = 0.5 * math.pi
  c, s = np.cos(theta), np.sin(theta)
  A = np.array([[c, s , 0], [-s, c , 0], [0, 0, 1]])
  AD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
  ADD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
  b = A.dot(b) # avoid constant flow by applying translation first x'=A(x+b)=Ax+Ab
  return (b, bD, bDD, A, AD, ADD)

# constant movement from [-1,0] to [1,0] in a straight line
def constantLinearTranslation(t, numberOfTimeSteps):
  b = np.array([-0.5 + 2. / numberOfTimeSteps * t, 0, 0])
  bD = np.array([2. / numberOfTimeSteps, 0, 0])
  bDD = np.array([0, 0, 0])
  A = np.array([[1, 0 , 0], [0, 1 , 0], [0, 0, 1]])
  AD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
  ADD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
#  print b,bD
  return (b, bD, bDD, A, AD, ADD)

# movement from [0,0] to [0.5,0] to [-0.5,0] and back to [0,0] in a straight line
def acceleratedLinearTranslation(t, numberOfTimeSteps):
  theta = 2 * math.pi / numberOfTimeSteps
  c, s = np.cos(theta * t), np.sin(theta * t)
#  b = [0.5 * np.sin(2 * math.pi / numberOfTimeSteps * t), 0, 0]
#  bD = [0.5 * 2 * math.pi / numberOfTimeSteps * np.cos(2 * math.pi / numberOfTimeSteps * t), 0, 0]
  b = 0.5 * np.array([s + 1, 0, 0])
  bD = 0.5 * np.array([c, 0, 0]) * theta
  bDD = 0.5 * np.array([-s, 0, 0]) * theta * theta
  A = np.array([[1, 0 , 0], [0, 1 , 0], [0, 0, 1]])
  AD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
  ADD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
#  print np.array([-0.5, 0, 0]) + b
  return (b, bD, bDD, A, AD, ADD)

# movement with constant velocity once around the circle with radius 0.5 and center [0,0]
def constantTranslation(t, numberOfTimeSteps):
  theta = 2 * math.pi / numberOfTimeSteps
  c, s = np.cos(theta * t), np.sin(theta * t)
  b = 0.5 * np.array([s + 1, c, 0])
  bD = 0.5 * np.array([c, -s, 0]) * theta
  bDD = 0.5 * np.array([-s, -c, 0]) * theta * theta
  A = np.array([[1, 0 , 0], [0, 1 , 0], [0, 0, 1]])
  AD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
  ADD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
  return (b, bD, bDD, A, AD, ADD)

# movement with linearly increasing velocity around the circle with radius 0.5 and center [0,0]
def acceleratedTranslation(t, numberOfTimeSteps):
  theta = 2 * math.pi / numberOfTimeSteps / numberOfTimeSteps
  c, s = np.cos(theta * t * t), np.sin(theta * t * t)
  b = 0.5 * np.array([s + 1, c, 0])
  bD = 0.5 * np.array([c, -s, 0]) * 2 * theta * t
  bDD = 0.5 * np.array([-s, -c, 0]) * 2 * theta * t * 2 * theta * t + 0.5 * np.array([ c, -s, 0]) * 2 * theta
  A = np.array([[1, 0 , 0], [0, 1 , 0], [0, 0, 1]])
  AD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
  ADD = np.array([[0, 0 , 0], [0, 0 , 0], [0, 0, 0]])
#  print np.array([-0.5, 0, 0]) + b
  return (b, bD, bDD, A, AD, ADD)

# rotation with constant angular velocity once around the circle with radius 0.5 and center [0,0]
def constantRotation(t, numberOfTimeSteps):
  b = np.array([0, 0, 0])
  bD = np.array([0, 0, 0])
  bDD = np.array([0, 0, 0])
  theta = 2 * math.pi / numberOfTimeSteps
  c, s = np.cos(theta * t), np.sin(theta * t)
  A = np.array([[c, s , 0], [-s, c , 0], [0, 0, 1]])
  AD = np.array([[-s, c, 0], [-c, -s , 0], [0, 0, 0]]) * theta
  ADD = np.array([[-c, -s, 0], [s, -c , 0], [0, 0, 0]]) * theta * theta
  #  print A.dot( np.array([-0.5, 0, 0]))
  return (b, bD, bDD, A, AD, ADD)

# rotation with linearly increasing angular velocity once around the circle with radius 0.5 and center [0,0]
def acceleratedRotation(t, numberOfTimeSteps):
  b = np.array([0, 0, 0])
  bD = np.array([0, 0, 0])
  bDD = np.array([0, 0, 0])
  theta = 2 * math.pi / numberOfTimeSteps / numberOfTimeSteps
  c, s = np.cos(theta * t * t), np.sin(theta * t * t)
  A = np.array([[c, s ,0], [-s, c ,0], [0, 0, 1]])
  AD = np.array([[ -s, c ,0], [-c, -s , 0], [0, 0, 0]]) * 2 * theta * t
  ADD = np.array([[-c, -s, 0], [s, -c , 0], [0, 0, 0]]) * 2 * theta * t * 2 * theta * t + np.array([[ -s, c ,0], [-c, -s , 0], [0, 0, 0]]) * 2 * theta
  #  print A.dot( np.array([-0.5, 0, 0]))
  return (b, bD, bDD, A, AD, ADD)


transformationOptions = {"Nothing" : nothing, "n" : nothing, \
"Steady" : steady, "s" : steady, \
"ConstantTranslation": constantTranslation, "ct": constantTranslation, \
"AcceleratedTranslation": acceleratedTranslation, "at": acceleratedTranslation, \
"ConstantLinearTranslation": constantLinearTranslation, "clt": constantLinearTranslation, \
"AcceleratedLinearTranslation": acceleratedLinearTranslation, "alt": acceleratedLinearTranslation, \
"ConstantRotation": constantRotation, "cr": constantRotation, \
"AcceleratedRotation": acceleratedRotation, "ar": acceleratedRotation}

# the function and its derivatives
def evaluateFunction(x, t, field, numberOfTimeSteps):
  # this moves the center of the function to [-0.5,0,0] so that rotational symmetry is broken
  x1 = np.array([x[0] + 0.5, x[1], x[2]])
  x2 = np.array([x[0] - 0.5, x[1], x[2]])
  e = math.exp(-2 * np.linalg.norm(x1)) * 20 / numberOfTimeSteps
  if np.linalg.norm(x1) < 1e-10:
    eD = 0
  else:
    eD = -np.array([x1[0], x1[1], x1[2]]) * e * 2. / np.linalg.norm(x1)
  v1, J1, vt1 = fieldOptions[field](x1, t, numberOfTimeSteps)
  
#  speedFactor = 0.1
#  e = speedFactor
#  eD = [0,0,0]
#  if np.linalg.norm(x) > 1:
#    e = 0 # outerZero
#    e = 1e-10 / np.linalg.norm(x1) # outerDrop
#    e = math.exp(-2 * np.linalg.norm(x)) * 20 / numberOfTimeSteps
#    e = speedFactor / np.linalg.norm(x) # outerConstOld
#    e = math.exp(-2 * np.linalg.norm(x1)) # outerGaussian
#    e = speedFactor * math.exp(-2 * np.linalg.norm(x)) / math.exp(-2) # outerCentralGaussian
#    e = speedFactor * math.exp(-20 * np.linalg.norm(x)) / math.exp(-20) # outerCentralGaussianRapid
#    e = speedFactor * np.linalg.norm(x/np.linalg.norm(x) + ([0.5,0,0])) / np.linalg.norm(x1) # outerConst
#    e = speedFactor / np.linalg.norm(x1) # outerCentralConst

#    e = speedFactor * np.linalg.norm(x/np.linalg.norm(x) + ([0.5,0,0])) * np.linalg.norm(x/np.linalg.norm(x) + ([0.5,0,0])) / np.linalg.norm(x1) / np.linalg.norm(x1) # outer1/x
#    e = speedFactor * np.linalg.norm(x/np.linalg.norm(x) + ([0.5,0,0])) / np.linalg.norm(x1) / np.linalg.norm(x) # outerCentral1/x
#    e = speedFactor * np.linalg.norm(x/np.linalg.norm(x) + ([0.5,0,0])) / np.linalg.norm(x1) / pow(np.linalg.norm(x),10) # outerCentral1/x^10

#    e = 0.2 * np.linalg.norm(x/np.linalg.norm(x) + ([0.5,0,0])) / np.linalg.norm(x1) * (-1./3*np.linalg.norm(x)+1+1./3) # outerLinear
#    e = 0.2 / np.linalg.norm(x1) * (-1./3*np.linalg.norm(x1)+1) # outerCentralLinear
#  e = math.exp(-2 * np.linalg.norm(x)) # centralGaussian
#  e = math.exp(-2 * np.linalg.norm(x2)) # oppositeGaussian
#
#  # linear example precise
#  v1, J1, vt1 = fieldOptions[field](x, t, numberOfTimeSteps)
#  e = 1. / numberOfTimeSteps
#  if np.linalg.norm(x) > 1 and np.linalg.norm(x) <= 2: e = (2 / np.linalg.norm(x) - 1) / numberOfTimeSteps
#  if np.linalg.norm(x) > 2: e = 1e-10
#
#  # linear example wrong
#  v1, J1, vt1 = fieldOptions[field](x, t, numberOfTimeSteps)
#  e = 0.1 / numberOfTimeSteps
#  if np.linalg.norm(x) > 1 and np.linalg.norm(x) <= 2: e = (1 - 0.9 / np.linalg.norm(x)) / numberOfTimeSteps
#  if np.linalg.norm(x) > 2 and np.linalg.norm(x) <= 3.1: e = (3.1 / np.linalg.norm(x) - 1) / numberOfTimeSteps
#  if np.linalg.norm(x) > 3.1: e = 1e-10
#
#  # linear example wrong new
#  v1, J1, vt1 = fieldOptions[field](x, t, numberOfTimeSteps)
#  e = 0.1 / numberOfTimeSteps
#  if np.linalg.norm(x) > 0.5 and np.linalg.norm(x) <= 2: e = (1 - 0.45 / np.linalg.norm(x)) / numberOfTimeSteps
#  if np.linalg.norm(x) > 2 and np.linalg.norm(x) <= 3.55: e = (3.55 / np.linalg.norm(x) - 1) / numberOfTimeSteps
#  if np.linalg.norm(x) > 3.55: e = 1e-10

  v = v1 * e
  J = J1 * e + np.outer(v1,eD)
  vt = vt1 * e
  return (v, J, vt)

def evaluateFunctionWithoutChange(x, t, field, numberOfTimeSteps):
  return fieldOptions[field](x, t, numberOfTimeSteps)

# coordinate transform x' = A(t)x + b(t) applied to the vector field in evaluateFunction
# the transformed field has the shape v' = A v + AD x + bd
# v'(x) = A v(A^{-1}(x-b)) + AD A^{-1}(x-b) + bD
# the jacobian suffices: J' = A J A^T + AD A^T
# the temporal derivative suffices v_t' = A v_t - A J A^T AD x + AD v - A J A^T bD + ADD x - AD A^T AD x + bDD - AD A^T bD
def transformFunction(x, t, field, transformation, numberOfTimeSteps):
  b, bD, bDD, A, AD, ADD = transformationOptions[transformation](t, numberOfTimeSteps)

  x2 = np.transpose(A).dot(x - b)
  v2, J2, vt2 = evaluateFunction(x2, t, field, numberOfTimeSteps)
  v = A.dot(v2) + AD.dot(x2) + bD
  # pseudo
#  v = A.dot(v2)
  J = (A.dot(J2) + AD).dot(np.transpose(A))

  vt = A.dot(vt2) - A.dot(J2).dot(np.transpose(A)).dot(AD).dot(x2) + AD.dot(v2) - A.dot(J2).dot(np.transpose(A)).dot(bD) + ADD.dot(x2) - AD.dot(np.transpose(A)).dot(AD).dot(x2) + bDD - AD.dot(np.transpose(A)).dot(bD)
  return (v, J, vt)



