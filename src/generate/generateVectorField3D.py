# this script produces a 3D time-depenendent vector field on [-1,1]^3 from an anlytic formula as a vti file
# if extentSize is 1, the domain is [-4,4]^3. that is used to keep the pathlines seeded in [-1,1]^3 within the domain and prevent boundary artefacts


import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import inv
from helper import *



#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 4:
  print ("use: generateVectorField2D.py <filepath to folder that will contain the data> <type of field: center, saddle, sink, or source> <type of transformation: steady, constantTranslation, acceleratedTranslation, constantLinearTranslation, acceleratedLinearTranslation, constantRotation, or acceleratedRotation>")
  sys.exit()

input = sys.argv[1]
if not os.path.isdir(input):
  os.makedirs(input)

field = sys.argv[2]
transformation = sys.argv[3]
#print field, transformation

if len(sys.argv) > 4:
  extentSize = sys.argv[4]
else:
  extentSize = 0

#-----------------------------------------------------------------------------------
# paramters
numberOfTimeSteps = 20
resolution = 20
print ("the parameters of this run of generateVectorField3D are:")
print ("numberOfTimeSteps =", numberOfTimeSteps)
print ("resolution =", resolution)
print ("extentSize =", extentSize)

#-----------------------------------------------------------------------------------
# main program

for j in range(numberOfTimeSteps + 1):
  dataSet = vtk.vtkImageData()
  dataSet.SetOrigin(0,0,0)
  dataSet.SetExtent(-int(resolution/2), int(resolution/2), -int(resolution/2), int(resolution/2), -int(resolution/2), int(resolution/2))
  if extentSize == '1':
    dataSet.SetExtent(-2*resolution, 2*resolution, -2*resolution, 2*resolution, -2*resolution, 2*resolution)
  dataSet.SetSpacing(2./resolution, 2./resolution, 2./resolution)
#  print dataSet.GetBounds()

  vectors = vtk.vtkDoubleArray()
  vectors.SetNumberOfComponents(3)
  vectors.SetNumberOfTuples(dataSet.GetNumberOfPoints())
  vectors.SetName("vectors")
  dataSet.GetPointData().SetVectors(vectors)

  for i in range(dataSet.GetNumberOfPoints()):
    v, J, vt = transformFunction(np.array(dataSet.GetPoint(i)), j, field, transformation, numberOfTimeSteps)
    vectors.SetTuple3( i, v[0], v[1], v[2]  );

  writer = vtk.vtkXMLImageDataWriter()
  writer.SetFileName(input + "/data" + str(j) + ".vti")
  writer.SetInputData(dataSet)
  writer.Write()
