# trace generated using paraview version 5.6.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
import os
import re
from paraview.simple import *
LoadPlugin('libSurfaceLIC.dylib', remote=True, ns=globals())

def sorted_aphanumeric(data):
  convert = lambda text: int(text) if text.isdigit() else text.lower()
  alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
  return sorted(data, key=alphanum_key)


#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

folder = sys.argv[1]
field = sys.argv[2]
transformation = sys.argv[3]
pathName = folder + '/data0.vti'
fileList = sorted_aphanumeric(os.listdir(folder))
pathList = []
for i in fileList:
  filename, file_extension = os.path.splitext(i)
  if (file_extension == ".vti"):
    pathList.append(folder + '/' + i)
#print pathList

# create a new 'XML Image Data Reader'
data0vti = XMLImageDataReader(FileName=pathList)
data0vti.PointArrayStatus = ['vectors']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1000, 1000]

# show data in view
data0vtiDisplay = Show(data0vti, renderView1)

# get color transfer function/color map for 'vectors'
vectorsLUT = GetColorTransferFunction('vectors')

# get opacity transfer function/opacity map for 'vectors'
vectorsPWF = GetOpacityTransferFunction('vectors')

# trace defaults for the display properties.
data0vtiDisplay.Representation = 'Slice'
data0vtiDisplay.ColorArrayName = ['POINTS', 'vectors']
data0vtiDisplay.LookupTable = vectorsLUT
data0vtiDisplay.OSPRayScaleArray = 'vectors'
data0vtiDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
data0vtiDisplay.SelectOrientationVectors = 'vectors'
data0vtiDisplay.ScaleFactor = 0.2
data0vtiDisplay.SelectScaleArray = 'None'
data0vtiDisplay.GlyphType = 'Arrow'
data0vtiDisplay.GlyphTableIndexArray = 'None'
data0vtiDisplay.GaussianRadius = 0.01
data0vtiDisplay.SetScaleArray = ['POINTS', 'vectors']
data0vtiDisplay.ScaleTransferFunction = 'PiecewiseFunction'
data0vtiDisplay.OpacityArray = ['POINTS', 'vectors']
data0vtiDisplay.OpacityTransferFunction = 'PiecewiseFunction'
data0vtiDisplay.DataAxesGrid = 'GridAxesRepresentation'
data0vtiDisplay.SelectionCellLabelFontFile = ''
data0vtiDisplay.SelectionPointLabelFontFile = ''
data0vtiDisplay.PolarAxes = 'PolarAxesRepresentation'
data0vtiDisplay.ScalarOpacityUnitDistance = 0.3838766207332969
data0vtiDisplay.ScalarOpacityFunction = vectorsPWF
data0vtiDisplay.SelectInputVectors = ['POINTS', 'vectors']
data0vtiDisplay.WriteLog = ''

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
data0vtiDisplay.DataAxesGrid.XTitleFontFile = ''
data0vtiDisplay.DataAxesGrid.YTitleFontFile = ''
data0vtiDisplay.DataAxesGrid.ZTitleFontFile = ''
data0vtiDisplay.DataAxesGrid.XLabelFontFile = ''
data0vtiDisplay.DataAxesGrid.YLabelFontFile = ''
data0vtiDisplay.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
data0vtiDisplay.PolarAxes.PolarAxisTitleFontFile = ''
data0vtiDisplay.PolarAxes.PolarAxisLabelFontFile = ''
data0vtiDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
data0vtiDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# reset view to fit data
renderView1.ResetCamera()

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]

# get the material library
materialLibrary1 = GetMaterialLibrary()

# show color bar/color legend
data0vtiDisplay.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# change representation type
data0vtiDisplay.SetRepresentationType('Surface LIC')

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
vectorsLUT.ApplyPreset('Spectral_lowBlue', True)

# rescale color and/or opacity maps used to exactly fit the current data range
data0vtiDisplay.RescaleTransferFunctionToDataRange(False, True)

# Properties modified on data0vtiDisplay
data0vtiDisplay.NumberOfSteps = 200

# Properties modified on data0vtiDisplay
data0vtiDisplay.ColorMode = 'Multiply'

# Properties modified on data0vtiDisplay
data0vtiDisplay.EnhanceContrast = 'LIC and Color'

# Properties modified on data0vtiDisplay
data0vtiDisplay.GenerateNoiseTexture = 1

# Properties modified on data0vtiDisplay
data0vtiDisplay.NoiseType = 'Perlin'

# Properties modified on data0vtiDisplay
data0vtiDisplay.NoiseTextureSize = 400

# Properties modified on data0vtiDisplay
data0vtiDisplay.NoiseGrainSize = 32

# create a new 'Glyph'
glyph1 = Glyph(Input=data0vti,
    GlyphType='Arrow')
glyph1.OrientationArray = ['POINTS', 'vectors']
glyph1.ScaleArray = ['POINTS', 'No scale array']
glyph1.ScaleFactor = 0.2
glyph1.GlyphTransform = 'Transform2'

# show data in view
glyph1Display = Show(glyph1, renderView1)

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = [None, '']
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'None'
glyph1Display.ScaleFactor = 0.2263717293739319
glyph1Display.SelectScaleArray = 'None'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'None'
glyph1Display.GaussianRadius = 0.011318586468696594
glyph1Display.SetScaleArray = [None, '']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = [None, '']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.SelectionCellLabelFontFile = ''
glyph1Display.SelectionPointLabelFontFile = ''
glyph1Display.PolarAxes = 'PolarAxesRepresentation'
glyph1Display.SelectInputVectors = [None, '']
glyph1Display.WriteLog = ''

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
glyph1Display.DataAxesGrid.XTitleFontFile = ''
glyph1Display.DataAxesGrid.YTitleFontFile = ''
glyph1Display.DataAxesGrid.ZTitleFontFile = ''
glyph1Display.DataAxesGrid.XLabelFontFile = ''
glyph1Display.DataAxesGrid.YLabelFontFile = ''
glyph1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
glyph1Display.PolarAxes.PolarAxisTitleFontFile = ''
glyph1Display.PolarAxes.PolarAxisLabelFontFile = ''
glyph1Display.PolarAxes.LastRadialAxisTextFontFile = ''
glyph1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1
glyph1.ScaleArray = ['POINTS', 'vectors']

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1
glyph1.ScaleFactor = 0.5

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.TipResolution = 2

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(data0vti)

# change representation type
data0vtiDisplay.SetRepresentationType('Surface')

# set active source
SetActiveSource(glyph1)

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.ShaftResolution = 2

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.TipRadius = 0.3

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.TipLength = 0.3

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.ShaftRadius = 0.1

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on glyph1
glyph1.MaximumNumberOfSamplePoints = 100

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(data0vti)

# change representation type
data0vtiDisplay.SetRepresentationType('Surface LIC')

# set active source
SetActiveSource(glyph1)

# Properties modified on glyph1
glyph1.MaximumNumberOfSamplePoints = 50

# update the view to ensure updated data information
renderView1.Update()

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 1

# save screenshot
SaveScreenshot(folder + '/' + str(field) + str(transformation) + 'Lic.png', renderView1, ImageResolution=[1000, 1000])

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# save animation
SaveAnimation(folder + '/' + 'Lic.png', renderView1, ImageResolution=[1000, 1000], FrameRate=5)

submitCommand = 'ffmpeg -y -r 5 -i ' + folder + '/Lic.0%03d.png -vcodec libx264 -r 30 -pix_fmt yuv420p ' + folder + '/' + str(field) + str(transformation) + 'LicMovie.mp4'
print ('--submitting: ' + submitCommand)
os.system(submitCommand)

for f in os.listdir(folder):
  if f.find('Lic') == 0:
    os.remove(folder + '/' + f)

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]
renderView1.CameraParallelScale = 1.4142135623730947

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
