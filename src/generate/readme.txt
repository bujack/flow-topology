This command produces the whole test suite including images:

vtkpython flowTopology/src/generate/testSuiteAll.py flowTopology/src/generate flowTopology/testSuite



Run generateVectorField2D.py through vtkpython:
vtk/build/bin/vtkpython flowTopology/src/generate/generateVectorField2D.py data/test field transformation

where field and transformation come from the following options

fieldOptions = {"center": center, "saddle": saddle, "sink": sink, "source": source}

transformationOptions = {"steady" : steady, "s" : steady, \
"constantTranslation": constantTranslation, "ct": constantTranslation, \
"acceleratedTranslation": acceleratedTranslation, "at": acceleratedTranslation, \
"constantLinearTranslation": constantLinearTranslation, "clt": constantLinearTranslation, \
"acceleratedLinearTranslation": acceleratedLinearTranslation, "alt": acceleratedLinearTranslation, \
"constantRotation": constantRotation, "cr": constantRotation, \
"acceleratedRotation": acceleratedRotation, "ar": acceleratedRotation}



You can load a visualization with the ParaView state file:
flowTopology/paraViewScripts/lic.pvsm
ParaView will ask for the filepath



A movie can be produced through the ParaView script:
flowTopology/paraViewScripts/generateLicMovie.py
The filepath needs to be replaced everywhere in the script if you don't run it for test



For 3D
vtk/build/bin/vtkpython flowTopology/src/generate/generateVectorField3D.py data/test field transformation


Explicit does not transform the data or weigh it with the gaussian. it just directly spits out the dataset from the formula

vtkpython flowTopology/src/generate/generateVectorFieldExplicit.py flowTopology/sandbox/data/gyre