# this script converts a dataset from our test suite into space time for tobias algorithm, i.e. 2D vector plus time in z-direction

import vtk
import numpy as np
import sys
import math
import os
from numpy.linalg import eig
import vtk.util.numpy_support as VN
from helper import *


#-----------------------------------------------------------------------------------
# paramters
numberOfTimeSteps = 10
resolution = 50
d = 3
print ("the parameters of this run are:")
print ("numberOfTimeSteps =", numberOfTimeSteps)
print ("resolution =", resolution)
print ("length of a vector =", d)

#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) != 4:
  print ("use: generateSpaceTime.py <filepath to folder that will contain the data> <type of field: center, saddle, sink, or source> <type of transformation: steady, constantTranslation, acceleratedTranslation, constantLinearTranslation, acceleratedLinearTranslation, constantRotation, or acceleratedRotation>")
  sys.exit()

input = sys.argv[1]
if not os.path.isdir(input):
  os.makedirs(input)

field = sys.argv[2]
transformation = sys.argv[3]


#-----------------------------------------------------------------------------------
# main program
spaceTimeData = vtk.vtkImageData()
spaceTimeData.SetOrigin(0,0,0)
spaceTimeData.SetExtent(-resolution/2, resolution/2, -resolution/2, resolution/2, 0, numberOfTimeSteps-1)
spaceTimeData.SetSpacing(2./resolution, 2./resolution, 1)

spaceTimeV = np.zeros((numberOfTimeSteps, resolution+1, resolution+1, 3))
spaceTimeJ = np.zeros((numberOfTimeSteps, resolution+1, resolution+1, 3, 3))
spaceTimeVx = np.zeros((numberOfTimeSteps, resolution+1, resolution+1, d))
spaceTimeVy = np.zeros((numberOfTimeSteps, resolution+1, resolution+1, d))
spaceTimeVt = np.zeros((numberOfTimeSteps, resolution+1, resolution+1, 3))

for i in range(resolution+1):
  for j in range(resolution+1):
    x = np.array(spaceTimeData.GetPoint(i + j*(resolution+1)))
    for t in range(numberOfTimeSteps):
      spaceTimeV[t,j,i], spaceTimeJ[t,j,i], spaceTimeVt[t,j,i] = transformFunction(x, t, field, transformation, numberOfTimeSteps)
#      spaceTimeV[t,j,i,0:d] = transformFunction(x, t, field, transformation, numberOfTimeSteps)[0:d]
#      vD = transformVD(x, t, field, transformation, numberOfTimeSteps)
#      spaceTimeVx[t,j,i,0:d] = vD[0:d,0]
#      spaceTimeVy[t,j,i,0:d] = vD[0:d,1]
#      spaceTimeVt[t,j,i,0:d] = vD[0:d,2]

spaceTimeArrayV = VN.numpy_to_vtk(spaceTimeV[:,:,:,0:d].reshape(spaceTimeData.GetNumberOfPoints(),d))
spaceTimeArrayV.SetName("v")
spaceTimeData.GetPointData().AddArray(spaceTimeArrayV)

spaceTimeArrayVx = VN.numpy_to_vtk(spaceTimeJ[:,:,:,0:d,0].reshape(spaceTimeData.GetNumberOfPoints(),d))
spaceTimeArrayVx.SetName("vx")
spaceTimeData.GetPointData().AddArray(spaceTimeArrayVx)

spaceTimeArrayVy = VN.numpy_to_vtk(spaceTimeJ[:,:,:,0:d,1].reshape(spaceTimeData.GetNumberOfPoints(),d))
spaceTimeArrayVy.SetName("vy")
spaceTimeData.GetPointData().AddArray(spaceTimeArrayVy)

spaceTimeArrayVt = VN.numpy_to_vtk(spaceTimeVt[:,:,:,0:d].reshape(spaceTimeData.GetNumberOfPoints(),d))
spaceTimeArrayVt.SetName("vt")
spaceTimeData.GetPointData().AddArray(spaceTimeArrayVt)

writer = vtk.vtkXMLImageDataWriter()
writer.SetInputData(spaceTimeData)
writer.SetFileName(input + '/spaceTimeGenerated.vti' )
writer.Write()
