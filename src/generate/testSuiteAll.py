# this script coputes the full test suite: 2D, 3D
# for the extended version, change the dimensions in generateVectorField

import vtk
import numpy as np
import sys
import math
import os
import itertools

#-----------------------------------------------------------------------------------
# interpret input
if len(sys.argv) < 2:
  print ("use: testSuiteAll.py <filepath to output folder>")
  sys.exit()

output = sys.argv[1]
if not os.path.isdir(output):
  print ("enter filepath to output folder")
  sys.exit()

scriptPath = os.path.dirname(sys.argv[0])

transformationOptions = ["AcceleratedLinearTranslation"]
transformationOptions = ["Nothing", "Steady", "ConstantTranslation", "AcceleratedTranslation", "ConstantLinearTranslation", "AcceleratedLinearTranslation", "ConstantRotation", "AcceleratedRotation"]

fieldOptions = ["Center"]
fieldOptions = ["Center", "Saddle", "Sink", "Source"]

# Run over all cross product of parameters and submit all the jobs
# 2D
for x in itertools.product(fieldOptions, transformationOptions):
  folderName = output + '/2D/' + str(x[0]) + str(x[1])
  if not os.path.isdir(folderName):
    os.makedirs(folderName)

  submitCommand = 'vtkpython ' + scriptPath + '/generateVectorField2D.py ' + folderName + ' ' + str(x[0]) + ' ' + str(x[1]) + ' 0'
  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)

  submitCommand = 'pvpython ' + scriptPath + '/licOne2D.py ' + folderName + ' ' + str(x[0]) + ' ' + str(x[1])
  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)

  submitCommand = 'zip -r -j ' + folderName + '/' + str(x[0]) + str(x[1]) + '.zip ' + folderName + '/*.vti'
  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)

# 3D
for x in itertools.product(fieldOptions, transformationOptions):
  folderName = output + '/3D/' + str(x[0]) + str(x[1])
  if not os.path.isdir(folderName):
    os.makedirs(folderName)

  submitCommand = 'vtkpython ' + scriptPath + '/generateVectorField3D.py ' + folderName + ' ' + str(x[0]) + ' ' + str(x[1]) + ' 0'

  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)

  submitCommand = 'pvpython ' + scriptPath + '/licOne3D.py ' + folderName + ' ' + str(x[0]) + ' ' + str(x[1])
  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)

  submitCommand = 'zip -r -j ' + folderName + '/' + str(x[0]) + str(x[1]) + '.zip ' + folderName + '/*.vti'
  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)

# 2D Extended
for x in itertools.product(fieldOptions, transformationOptions):
  folderName = output + '/2DExtended/' + str(x[0]) + str(x[1])
  if not os.path.isdir(folderName):
    os.makedirs(folderName)

  submitCommand = 'vtkpython ' + scriptPath + '/generateVectorField2D.py ' + folderName + ' ' + str(x[0]) + ' ' + str(x[1]) + ' 1'
  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)

  submitCommand = 'zip -r -j ' + folderName + '/' + str(x[0]) + str(x[1]) + '.zip ' + folderName + '/*.vti'
  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)

# 3D Extended
for x in itertools.product(fieldOptions, transformationOptions):
  folderName = output + '/3DExtended/' + str(x[0]) + str(x[1])
  if not os.path.isdir(folderName):
    os.makedirs(folderName)

  submitCommand = 'vtkpython ' + scriptPath + '/generateVectorField3D.py ' + folderName + ' ' + str(x[0]) + ' ' + str(x[1]) + ' 1'

  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)

  submitCommand = 'zip -r -j ' + folderName + '/' + str(x[0]) + str(x[1]) + '.zip ' + folderName + '/*.vti'
  print ('--submitting: ' + submitCommand)
  os.system(submitCommand)
